FROM node:latest

ADD  __sapper__ /clux/__sapper__
ADD static /clux/static
ADD src /clux/static
ADD static /clux/static
ADD package.json /clux
ADD rollup.config.js /clux

WORKDIR /clux

RUN npm install

CMD ["node", "__sapper__/build"]
