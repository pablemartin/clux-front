'use strict';

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var sirv = _interopDefault(require('sirv'));
var polka = _interopDefault(require('polka'));
var compression = _interopDefault(require('compression'));
var fs = _interopDefault(require('fs'));
var path = _interopDefault(require('path'));
var Stream = _interopDefault(require('stream'));
var http = _interopDefault(require('http'));
var Url = _interopDefault(require('url'));
var https = _interopDefault(require('https'));
var zlib = _interopDefault(require('zlib'));

function noop() { }
function run(fn) {
    return fn();
}
function blank_object() {
    return Object.create(null);
}
function run_all(fns) {
    fns.forEach(run);
}
function safe_not_equal(a, b) {
    return a != a ? b == b : a !== b || ((a && typeof a === 'object') || typeof a === 'function');
}
function subscribe(store, ...callbacks) {
    if (store == null) {
        return noop;
    }
    const unsub = store.subscribe(...callbacks);
    return unsub.unsubscribe ? () => unsub.unsubscribe() : unsub;
}
function get_store_value(store) {
    let value;
    subscribe(store, _ => value = _)();
    return value;
}
function custom_event(type, detail) {
    const e = document.createEvent('CustomEvent');
    e.initCustomEvent(type, false, false, detail);
    return e;
}

let current_component;
function set_current_component(component) {
    current_component = component;
}
function get_current_component() {
    if (!current_component)
        throw new Error(`Function called outside component initialization`);
    return current_component;
}
function onMount(fn) {
    get_current_component().$$.on_mount.push(fn);
}
function afterUpdate(fn) {
    get_current_component().$$.after_update.push(fn);
}
function createEventDispatcher() {
    const component = get_current_component();
    return (type, detail) => {
        const callbacks = component.$$.callbacks[type];
        if (callbacks) {
            // TODO are there situations where events could be dispatched
            // in a server (non-DOM) environment?
            const event = custom_event(type, detail);
            callbacks.slice().forEach(fn => {
                fn.call(component, event);
            });
        }
    };
}
function setContext(key, context) {
    get_current_component().$$.context.set(key, context);
}
const escaped = {
    '"': '&quot;',
    "'": '&#39;',
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;'
};
function escape(html) {
    return String(html).replace(/["'&<>]/g, match => escaped[match]);
}
const missing_component = {
    $$render: () => ''
};
function validate_component(component, name) {
    if (!component || !component.$$render) {
        if (name === 'svelte:component')
            name += ' this={...}';
        throw new Error(`<${name}> is not a valid SSR component. You may need to review your build config to ensure that dependencies are compiled, rather than imported as pre-compiled modules`);
    }
    return component;
}
let on_destroy;
function create_ssr_component(fn) {
    function $$render(result, props, bindings, slots) {
        const parent_component = current_component;
        const $$ = {
            on_destroy,
            context: new Map(parent_component ? parent_component.$$.context : []),
            // these will be immediately discarded
            on_mount: [],
            before_update: [],
            after_update: [],
            callbacks: blank_object()
        };
        set_current_component({ $$ });
        const html = fn(result, props, bindings, slots);
        set_current_component(parent_component);
        return html;
    }
    return {
        render: (props = {}, options = {}) => {
            on_destroy = [];
            const result = { title: '', head: '', css: new Set() };
            const html = $$render(result, props, {}, options);
            run_all(on_destroy);
            return {
                html,
                css: {
                    code: Array.from(result.css).map(css => css.code).join('\n'),
                    map: null // TODO
                },
                head: result.title + result.head
            };
        },
        $$render
    };
}
function add_attribute(name, value, boolean) {
    if (value == null || (boolean && !value))
        return '';
    return ` ${name}${value === true ? '' : `=${typeof value === 'string' ? JSON.stringify(escape(value)) : `"${value}"`}`}`;
}

const subscriber_queue = [];
/**
 * Create a `Writable` store that allows both updating and reading by subscription.
 * @param {*=}value initial value
 * @param {StartStopNotifier=}start start and stop notifications for subscriptions
 */
function writable(value, start = noop) {
    let stop;
    const subscribers = [];
    function set(new_value) {
        if (safe_not_equal(value, new_value)) {
            value = new_value;
            if (stop) { // store is ready
                const run_queue = !subscriber_queue.length;
                for (let i = 0; i < subscribers.length; i += 1) {
                    const s = subscribers[i];
                    s[1]();
                    subscriber_queue.push(s, value);
                }
                if (run_queue) {
                    for (let i = 0; i < subscriber_queue.length; i += 2) {
                        subscriber_queue[i][0](subscriber_queue[i + 1]);
                    }
                    subscriber_queue.length = 0;
                }
            }
        }
    }
    function update(fn) {
        set(fn(value));
    }
    function subscribe(run, invalidate = noop) {
        const subscriber = [run, invalidate];
        subscribers.push(subscriber);
        if (subscribers.length === 1) {
            stop = start(set) || noop;
        }
        run(value);
        return () => {
            const index = subscribers.indexOf(subscriber);
            if (index !== -1) {
                subscribers.splice(index, 1);
            }
            if (subscribers.length === 0) {
                stop();
                stop = null;
            }
        };
    }
    return { set, update, subscribe };
}

// import * as api from 'api.js';


function club_init() {
  const { subscribe, set, update } = writable(null);

  return {
    subscribe,
    set: (club) => update(data => club)
  };
}

function activity_init() {
  const { subscribe, set, update } = writable(null);

  function init(activity){
    // console.log(activity);
    // localStorage.setItem('activity', JSON.stringify(activity))
    // console.log(localStorage.getItem('activity'))
    // console.log( JSON.parse(localStorage.getItem('activity')) )
    set(activity);
  }

  return {
    subscribe,
    init: (activity) => init(activity),
    set: (activity) => update(data => activity)
  };
}

const club = club_init();
const activity = activity_init();

const user = {
  'email': writable(null),
  'password': writable(null),
  "logued": writable(false)
};

// async function get_users() {
//     const result = await http
//       .get(`user/`+user_id)
//     return result.data
//   }

//   async function get_user_activities() {
//     const result = await http
//       .get(`user/`+user_id+'/activities/')
//     user_activities = result.data.activities
//   }

//   async function get_activity(id) {
//     const result = await http
//       .get(`activity/`+id, 
//       {
//         auth: {
//           username: 'admincluxcom',
//           password: 'admin123'
//         },
//       })
    
//     return result.data
//   }

/* src/routes/index.svelte generated by Svelte v3.24.0 */

const Routes = create_ssr_component(($$result, $$props, $$bindings, $$slots) => {
	let $club = get_store_value(club);

	return `${($$result.head += `${($$result.title = `<title>${escape($club.name)}</title>`, "")}`, "")}

<div class="${"card"}" style="${"border: 0;"}"><div class="${"card-image"}"><img${add_attribute("src", $club.image, 0)} class="${"img-responsive"}" alt="${"img"}"></div>

  <div class="${"card-header text-center"}"><div class="${"card-title h1"}">${escape($club.name)}</div>
    <div class="${"card-subtitle"}"><span class="${"chip"}">${escape($club.direction)}</span><span class="${"chip"}">${escape($club.phone)}</span>
    	<p>${escape($club.bio)}</p></div></div>

  <div class="${"card-body"}"><div class="${"columns"}"><div class="${"column col-4"}"><img src="${"https://via.placeholder.com/300x350"}" alt="${"img"}" class="${"img-responsive"}"></div>
  		<div class="${"column col-2"}"><img src="${"https://via.placeholder.com/300x350"}" alt="${"img"}" class="${"img-responsive"}"></div>
  		<div class="${"column col-4"}"><img src="${"https://via.placeholder.com/300x350"}" alt="${"img"}" class="${"img-responsive"}"></div>
  		<div class="${"column col-2"}"><img src="${"https://via.placeholder.com/300x350"}" alt="${"img"}" class="${"img-responsive"}"></div></div></div>
  <div class="${"card-footer"}"></div></div>`;
});

var index = /*#__PURE__*/Object.freeze({
    __proto__: null,
    'default': Routes
});

/* node_modules/svelte-waypoint/src/Waypoint.svelte generated by Svelte v3.24.0 */

const css = {
	code: ".wrapper.svelte-142y8oi{display:inline-block}",
	map: "{\"version\":3,\"file\":\"Waypoint.svelte\",\"sources\":[\"Waypoint.svelte\"],\"sourcesContent\":[\"<script>\\n  import { createEventDispatcher, onDestroy } from 'svelte';\\n\\n  const dispatch = createEventDispatcher();\\n\\n  export let offset = 0;\\n  export let throttle = 250;\\n  export let c = '';\\n  export let style = '';\\n  export let once = true;\\n  export let threshold = 1.0;\\n  export let disabled = false;\\n\\n  let className = \\\"\\\";\\n  export { className as class };\\n\\n  let visible = disabled;\\n  let wasVisible = false;\\n  let intersecting = false;\\n  let removeHandlers = () => {};\\n\\n  function throttleFn(fn, time) {\\n    let last, deferTimer;\\n\\n    return () => {\\n      const now = +new Date;\\n\\n      if (last && now < last + time) {\\n        // hold on to it\\n        clearTimeout(deferTimer);\\n        deferTimer = setTimeout(function () {\\n          last = now;\\n          fn();\\n        }, time);\\n      } else {\\n        last = now;\\n        fn();\\n      }\\n    };\\n  }\\n\\n  function callEvents(wasVisible, observer, node) {\\n    if (visible && !wasVisible) {\\n      dispatch('enter');\\n      return;\\n    }\\n\\n    if (wasVisible && !intersecting) {\\n      dispatch('leave');\\n    }\\n\\n    if (once && wasVisible && !intersecting) {\\n      removeHandlers();\\n    }\\n  }\\n\\n  function waypoint(node) {\\n    if (!window || disabled) return;\\n\\n    if (window.IntersectionObserver && window.IntersectionObserverEntry) {\\n      const observer = new IntersectionObserver(([ { isIntersecting } ]) => {\\n        wasVisible = visible;\\n\\n        intersecting = isIntersecting;\\n\\n        if (wasVisible && once && !isIntersecting) {\\n          callEvents(wasVisible, observer, node);\\n          return;\\n        }\\n\\n        visible = isIntersecting;\\n\\n        callEvents(wasVisible, observer, node);\\n      }, {\\n        rootMargin: offset + 'px',\\n        threshold,\\n      });\\n\\n      observer.observe(node);\\n\\n      removeHandlers = () => observer.unobserve(node);\\n\\n      return removeHandlers;\\n    }\\n\\n    function checkIsVisible() {\\n      // Kudos https://github.com/twobin/react-lazyload/blob/master/src/index.jsx#L93\\n      if (!(node.offsetWidth || node.offsetHeight || node.getClientRects().length)) return;\\n\\n      let top;\\n      let height;\\n\\n      try {\\n        ({ top, height } = node.getBoundingClientRect());\\n      } catch (e) {\\n        ({ top, height } = defaultBoundingClientRect);\\n      }\\n\\n      const windowInnerHeight = window.innerHeight\\n        || document.documentElement.clientHeight;\\n\\n      wasVisible = visible;\\n      intersecting = (top - offset <= windowInnerHeight) &&\\n        (top + height + offset >= 0);\\n\\n      if (wasVisible && once && !isIntersecting) {\\n        callEvents(wasVisible, observer, node);\\n        return;\\n      }\\n\\n      visible = intersecting;\\n\\n      callEvents(wasVisible);\\n    }\\n\\n    checkIsVisible();\\n\\n    const throttled = throttleFn(checkIsVisible, throttle);\\n\\n    window.addEventListener('scroll', throttled);\\n    window.addEventListener('resize', throttled);\\n\\n    removeHandlers = () => {\\n      window.removeEventListener('scroll', throttled);\\n      window.removeEventListener('resize', throttled);\\n    }\\n\\n    return removeHandlers;\\n  }\\n</script>\\n\\n<style>\\n.wrapper {\\n  display: inline-block;\\n}\\n</style>\\n\\n<div class=\\\"wrapper {className} {c}\\\" {style} use:waypoint>\\n  {#if visible}\\n    <slot />\\n  {/if}\\n</div>\\n\"],\"names\":[],\"mappings\":\"AAoIA,QAAQ,eAAC,CAAC,AACR,OAAO,CAAE,YAAY,AACvB,CAAC\"}"
};

const Waypoint = create_ssr_component(($$result, $$props, $$bindings, $$slots) => {
	const dispatch = createEventDispatcher();
	let { offset = 0 } = $$props;
	let { throttle = 250 } = $$props;
	let { c = "" } = $$props;
	let { style = "" } = $$props;
	let { once = true } = $$props;
	let { threshold = 1 } = $$props;
	let { disabled = false } = $$props;
	let { class: className = "" } = $$props;
	let visible = disabled;

	if ($$props.offset === void 0 && $$bindings.offset && offset !== void 0) $$bindings.offset(offset);
	if ($$props.throttle === void 0 && $$bindings.throttle && throttle !== void 0) $$bindings.throttle(throttle);
	if ($$props.c === void 0 && $$bindings.c && c !== void 0) $$bindings.c(c);
	if ($$props.style === void 0 && $$bindings.style && style !== void 0) $$bindings.style(style);
	if ($$props.once === void 0 && $$bindings.once && once !== void 0) $$bindings.once(once);
	if ($$props.threshold === void 0 && $$bindings.threshold && threshold !== void 0) $$bindings.threshold(threshold);
	if ($$props.disabled === void 0 && $$bindings.disabled && disabled !== void 0) $$bindings.disabled(disabled);
	if ($$props.class === void 0 && $$bindings.class && className !== void 0) $$bindings.class(className);
	$$result.css.add(css);

	return `<div class="${"wrapper " + escape(className) + " " + escape(c) + " svelte-142y8oi"}"${add_attribute("style", style, 0)}>${visible
	? `${$$slots.default ? $$slots.default({}) : ``}`
	: ``}</div>`;
});

/* node_modules/svelte-image/src/Image.svelte generated by Svelte v3.24.0 */

const css$1 = {
	code: "img.svelte-373mer.svelte-373mer{object-position:center;position:absolute;top:0;left:0;width:100%;will-change:opacity}.blur.svelte-373mer.svelte-373mer{filter:blur(10px);transition:opacity 0.4s ease, filter 0.5s ease}.placeholder.svelte-373mer.svelte-373mer{opacity:1;transition:opacity 0.5s ease;transition-delay:0.7s}.main.svelte-373mer.svelte-373mer{opacity:0;transition:opacity 0.5s ease;transition-delay:0.7s}.loaded.svelte-373mer .placeholder.svelte-373mer{opacity:0}.loaded.svelte-373mer .main.svelte-373mer{opacity:1}",
	map: "{\"version\":3,\"file\":\"Image.svelte\",\"sources\":[\"Image.svelte\"],\"sourcesContent\":[\"<script>\\n  import Waypoint from \\\"svelte-waypoint\\\";\\n\\n  export let c = \\\"\\\"; // deprecated\\n  export let alt = \\\"\\\";\\n  export let width = \\\"\\\";\\n  export let height = \\\"\\\";\\n  export let src = \\\"\\\";\\n  export let srcset = \\\"\\\";\\n  export let srcsetWebp = \\\"\\\";\\n  export let ratio = \\\"100%\\\";\\n  export let blur = false;\\n  export let sizes = \\\"(max-width: 1000px) 100vw, 1000px\\\";\\n  export let threshold = 1.0;\\n  export let lazy = true;\\n  export let wrapperClass = \\\"\\\";\\n  export let placeholderClass = \\\"\\\";\\n\\n  let className = \\\"\\\";\\n  export { className as class };\\n\\n  let loaded = !lazy;\\n\\n  function load(img) {\\n    img.onload = () => loaded = true;\\n  }\\n</script>\\n\\n<style>\\n  img {\\n    object-position: center;\\n    position: absolute;\\n    top: 0;\\n    left: 0;\\n    width: 100%;\\n    will-change: opacity;\\n  }\\n\\n  .blur {\\n    filter: blur(10px);\\n    transition: opacity 0.4s ease, filter 0.5s ease;\\n  }\\n\\n  .placeholder {\\n    opacity: 1;\\n    transition: opacity 0.5s ease;\\n    transition-delay: 0.7s;\\n  }\\n   .main {\\n    opacity: 0;\\n    transition: opacity 0.5s ease;\\n    transition-delay: 0.7s;\\n  }\\n\\n  .loaded .placeholder {\\n    opacity: 0;\\n  }\\n  .loaded .main {\\n    opacity: 1;\\n  }\\n</style>\\n\\n<Waypoint\\n  class={wrapperClass}\\n  style=\\\"min-height: 100px; width: 100%\\\"\\n  once\\n  {threshold}\\n  disabled={!lazy}>\\n  <div class:loaded style=\\\"position: relative; width: 100%;\\\">\\n    <div style=\\\"position: relative; overflow: hidden\\\">\\n      <div style=\\\"width:100%;padding-bottom:{ratio};\\\" />\\n      <img\\n        class=\\\"placeholder {placeholderClass}\\\"\\n        {src}\\n        {alt}\\n      >\\n      <picture>\\n        <source type=\\\"image/webp\\\" srcset={srcsetWebp}>\\n        <source srcset={srcset}>\\n        <img\\n          use:load\\n          class=\\\"main {c} {className}\\\"\\n          class:blur\\n          {alt}\\n          {srcset}\\n          {width}\\n          {height}\\n          {sizes}\\n        >\\n    </picture>\\n    </div>\\n  </div>\\n</Waypoint>\"],\"names\":[],\"mappings\":\"AA6BE,GAAG,4BAAC,CAAC,AACH,eAAe,CAAE,MAAM,CACvB,QAAQ,CAAE,QAAQ,CAClB,GAAG,CAAE,CAAC,CACN,IAAI,CAAE,CAAC,CACP,KAAK,CAAE,IAAI,CACX,WAAW,CAAE,OAAO,AACtB,CAAC,AAED,KAAK,4BAAC,CAAC,AACL,MAAM,CAAE,KAAK,IAAI,CAAC,CAClB,UAAU,CAAE,OAAO,CAAC,IAAI,CAAC,IAAI,CAAC,CAAC,MAAM,CAAC,IAAI,CAAC,IAAI,AACjD,CAAC,AAED,YAAY,4BAAC,CAAC,AACZ,OAAO,CAAE,CAAC,CACV,UAAU,CAAE,OAAO,CAAC,IAAI,CAAC,IAAI,CAC7B,gBAAgB,CAAE,IAAI,AACxB,CAAC,AACA,KAAK,4BAAC,CAAC,AACN,OAAO,CAAE,CAAC,CACV,UAAU,CAAE,OAAO,CAAC,IAAI,CAAC,IAAI,CAC7B,gBAAgB,CAAE,IAAI,AACxB,CAAC,AAED,qBAAO,CAAC,YAAY,cAAC,CAAC,AACpB,OAAO,CAAE,CAAC,AACZ,CAAC,AACD,qBAAO,CAAC,KAAK,cAAC,CAAC,AACb,OAAO,CAAE,CAAC,AACZ,CAAC\"}"
};

const Image = create_ssr_component(($$result, $$props, $$bindings, $$slots) => {
	let { c = "" } = $$props; // deprecated
	let { alt = "" } = $$props;
	let { width = "" } = $$props;
	let { height = "" } = $$props;
	let { src = "" } = $$props;
	let { srcset = "" } = $$props;
	let { srcsetWebp = "" } = $$props;
	let { ratio = "100%" } = $$props;
	let { blur = false } = $$props;
	let { sizes = "(max-width: 1000px) 100vw, 1000px" } = $$props;
	let { threshold = 1 } = $$props;
	let { lazy = true } = $$props;
	let { wrapperClass = "" } = $$props;
	let { placeholderClass = "" } = $$props;
	let { class: className = "" } = $$props;
	let loaded = !lazy;

	if ($$props.c === void 0 && $$bindings.c && c !== void 0) $$bindings.c(c);
	if ($$props.alt === void 0 && $$bindings.alt && alt !== void 0) $$bindings.alt(alt);
	if ($$props.width === void 0 && $$bindings.width && width !== void 0) $$bindings.width(width);
	if ($$props.height === void 0 && $$bindings.height && height !== void 0) $$bindings.height(height);
	if ($$props.src === void 0 && $$bindings.src && src !== void 0) $$bindings.src(src);
	if ($$props.srcset === void 0 && $$bindings.srcset && srcset !== void 0) $$bindings.srcset(srcset);
	if ($$props.srcsetWebp === void 0 && $$bindings.srcsetWebp && srcsetWebp !== void 0) $$bindings.srcsetWebp(srcsetWebp);
	if ($$props.ratio === void 0 && $$bindings.ratio && ratio !== void 0) $$bindings.ratio(ratio);
	if ($$props.blur === void 0 && $$bindings.blur && blur !== void 0) $$bindings.blur(blur);
	if ($$props.sizes === void 0 && $$bindings.sizes && sizes !== void 0) $$bindings.sizes(sizes);
	if ($$props.threshold === void 0 && $$bindings.threshold && threshold !== void 0) $$bindings.threshold(threshold);
	if ($$props.lazy === void 0 && $$bindings.lazy && lazy !== void 0) $$bindings.lazy(lazy);
	if ($$props.wrapperClass === void 0 && $$bindings.wrapperClass && wrapperClass !== void 0) $$bindings.wrapperClass(wrapperClass);
	if ($$props.placeholderClass === void 0 && $$bindings.placeholderClass && placeholderClass !== void 0) $$bindings.placeholderClass(placeholderClass);
	if ($$props.class === void 0 && $$bindings.class && className !== void 0) $$bindings.class(className);
	$$result.css.add(css$1);

	return `${validate_component(Waypoint, "Waypoint").$$render(
		$$result,
		{
			class: wrapperClass,
			style: "min-height: 100px; width: 100%",
			once: true,
			threshold,
			disabled: !lazy
		},
		{},
		{
			default: () => `<div style="${"position: relative; width: 100%;"}" class="${["svelte-373mer", loaded ? "loaded" : ""].join(" ").trim()}"><div style="${"position: relative; overflow: hidden"}"><div style="${"width:100%;padding-bottom:" + escape(ratio) + ";"}"></div>
      <img class="${"placeholder " + escape(placeholderClass) + " svelte-373mer"}"${add_attribute("src", src, 0)}${add_attribute("alt", alt, 0)}>
      <picture><source type="${"image/webp"}"${add_attribute("srcset", srcsetWebp, 0)}>
        <source${add_attribute("srcset", srcset, 0)}>
        <img class="${[
				"main " + escape(c) + " " + escape(className) + " svelte-373mer",
				blur ? "blur" : ""
			].join(" ").trim()}"${add_attribute("alt", alt, 0)}${add_attribute("srcset", srcset, 0)}${add_attribute("width", width, 0)}${add_attribute("height", height, 0)}${add_attribute("sizes", sizes, 0)}></picture></div></div>`
		}
	)}`;
});

/* src/routes/activity.svelte generated by Svelte v3.24.0 */

const Activity = create_ssr_component(($$result, $$props, $$bindings, $$slots) => {
	let $club = get_store_value(club);
	let $activity = get_store_value(activity);

	return `<div class="${"panel"}" style="${"border: 0;"}"><div class="${"panel-header text-center"}"><figure class="${"avatar avatar-lg"}"><img${add_attribute("src", $club.image, 0)} alt="${"Avatar"}"></figure>
      <div class="${"panel-title h5 mt-10"}">${escape($activity.name)}</div>
      <div class="${"panel-subtitle"}">Profesora: Giselle Lorena Alarcon Principiantes, intermedias y avanzadas</div>
      <div class="${"chip"}"><i class="${"icon icon-message"}"></i></div>
      <div class="${"chip"}"><i class="${"icon icon-edit"}"></i></div></div>
    
    <div class="${"panel-body"}"><div class="${"columns"}"><div class="${"column col-12"}">
            <div class="${"tile tile-centered tile-partido"}"><div class="${"tile-partido-info"}"><small>01-01-2020 en cancha principal</small></div>
              <div class="${"tile-content"}"><div class="${"tile-title"}">${escape($club.short)} <strong>2</strong></div>
                <figure class="${"avatar avatar-lg"}"><img${add_attribute("src", $club.image, 0)} alt="${"Avatar"}"></figure>
                <figure class="${"avatar avatar-lg"}"><img${add_attribute("src", $club.image, 0)} alt="${"Avatar"}"></figure>
                <div class="${"tile-title"}"><strong>0</strong> A.V.L.</div></div></div></div>
          
          <div class="${"column col-xs-12 col-md-6"}"><div><small>Se encuentra en</small>
              <div class="${"chip"}"><strong>Cancha princial</strong></div></div>
          
            <div><h4>Partidos</h4>
              <div class="${"tile tile-centered"}"><div class="${"tile-content"}"><div class="${"tile-title"}">&gt; vs A.V.L</div>
                  <div class="${"tile-subtitle"}">Sabado 10 18:00hs Cancha principal.</div></div></div>
              <div class="${"tile tile-centered"}"><div class="${"tile-content"}"><div class="${"tile-title"}">&gt; vs Pinocho</div>
                  <div class="${"tile-subtitle"}">Sabado 17 18:00hs en Gascon 1111, CABA.</div></div></div>
              <a href="${"activity"}">Más partidos</a></div>

            <div><h4>Miembros</h4>
              <div class="${"tile tile-centered"}"><div class="${"tile-icon"}"><figure class="${"avatar avatar-md"}"><img${add_attribute("src", $club.image, 0)} alt="${"Avatar"}"></figure></div>
                <div class="${"tile-content"}"><div class="${"tile-title text-bold"}">Matias Hernan Perez</div>
                  <div class="${"tile-subtitle"}">Profesor</div></div></div>
              <div class="${"tile tile-centered"}"><div class="${"tile-icon"}"><figure class="${"avatar avatar-md"}"><img${add_attribute("src", $club.image, 0)} alt="${"Avatar"}"></figure></div>
                <div class="${"tile-content"}"><div class="${"tile-title text-bold"}">Ignacio Mazzitelli</div>
                  <div class="${"tile-subtitle"}">Jugador</div></div></div>
              <div class="${"tile tile-centered"}"><div class="${"tile-icon"}"><figure class="${"avatar avatar-md"}"><img${add_attribute("src", $club.image, 0)} alt="${"Avatar"}"></figure></div>
                <div class="${"tile-content"}"><div class="${"tile-title text-bold"}">Federico Mazzitelli</div>
                  <div class="${"tile-subtitle"}">Jugador</div></div></div></div>


            <div><h4>Horarios</h4>
              <div class="${"accordion"}"><input type="${"checkbox"}" id="${"accordion-1"}" name="${"accordion-checkbox"}" hidden>
                <label class="${"accordion-header"}" for="${"accordion-1"}"><i class="${"icon icon-arrow-right mr-1"}"></i>
                  Lunes 30
                </label>
                <div class="${"accordion-body"}"><div class="${"label"}">Clase 19:00hs a 20:00hs <span class="${"text-primary"}">categoria 90</span></div>
                  <div class="${"label"}">Clase 20:00hs a 24:00hs <span class="${"text-primary"}">categoria 91</span></div>
                  <div class="${"label"}">Clase 20:00hs a 24:00hs <span class="${"text-primary"}">categoria 91</span></div>
                  <div class="${"label"}">Clase 20:00hs a 24:00hs <span class="${"text-primary"}">categoria 91</span></div>
                  <div class="${"label"}">Clase 20:00hs a 24:00hs <span class="${"text-primary"}">categoria 91</span></div>
                  <div class="${"chip bg-primary"}">Partido vs AVL ! 20:00hs en Cancha Principal</div></div></div>

              <div class="${"accordion"}"><input type="${"checkbox"}" id="${"accordion-2"}" name="${"accordion-checkbox"}" hidden>
                <label class="${"accordion-header"}" for="${"accordion-2"}"><i class="${"icon icon-arrow-right mr-1"}"></i>
                  Martes
                </label>
                <div class="${"accordion-body"}"><div class="${"chip"}">Clase 19:00</div></div></div></div>

            <div><h4>Tarifas</h4>
              <span class="${"chip"}">120$ mensual</span></div>

            <div><h4>Categorias</h4>
              <div class="${"tile tile-centered"}"><div class="${"tile-content"}"><span class="${"chip"}">90</span>
                  <span class="${"chip"}">91</span>
                  <span class="${"chip"}">92</span>
                  <span class="${"chip"}">93</span>
                  <span class="${"chip"}">94</span>
                  <span class="${"chip"}">95</span></div></div></div></div>

          <div class="${"column col-xs-12 col-md-6"}"><div class="${"columns"}"><div class="${"column col-6"}">${validate_component(Image, "Image").$$render(
		$$result,
		{
			src: "https://via.placeholder.com/450x450",
			alt: "fuji",
			class: "img-responsive"
		},
		{},
		{}
	)}</div>
                <div class="${"column col-6"}">${validate_component(Image, "Image").$$render(
		$$result,
		{
			src: "https://via.placeholder.com/450x450",
			alt: "fuji",
			class: "img-responsive"
		},
		{},
		{}
	)}</div>
                <div class="${"column col-6"}">${validate_component(Image, "Image").$$render(
		$$result,
		{
			src: "https://via.placeholder.com/450x450",
			alt: "fuji",
			class: "img-responsive"
		},
		{},
		{}
	)}</div>
                <div class="${"column col-6"}">${validate_component(Image, "Image").$$render(
		$$result,
		{
			src: "https://via.placeholder.com/450x450",
			alt: "fuji",
			class: "img-responsive"
		},
		{},
		{}
	)}</div>
                <div class="${"column col-6"}">${validate_component(Image, "Image").$$render(
		$$result,
		{
			src: "https://via.placeholder.com/450x450",
			alt: "fuji",
			class: "img-responsive"
		},
		{},
		{}
	)}</div>
                <div class="${"column col-6"}">${validate_component(Image, "Image").$$render(
		$$result,
		{
			src: "https://via.placeholder.com/450x450",
			alt: "fuji",
			class: "img-responsive"
		},
		{},
		{}
	)}</div>
                <div class="${"column col-6"}">${validate_component(Image, "Image").$$render(
		$$result,
		{
			src: "https://via.placeholder.com/450x450",
			alt: "fuji",
			class: "img-responsive"
		},
		{},
		{}
	)}</div>
                <div class="${"column col-6"}">${validate_component(Image, "Image").$$render(
		$$result,
		{
			src: "https://via.placeholder.com/450x450",
			alt: "fuji",
			class: "img-responsive"
		},
		{},
		{}
	)}</div>
                <div class="${"column col-6"}">${validate_component(Image, "Image").$$render(
		$$result,
		{
			src: "https://via.placeholder.com/450x450",
			alt: "fuji",
			class: "img-responsive"
		},
		{},
		{}
	)}</div>
                <div class="${"column col-6"}">${validate_component(Image, "Image").$$render(
		$$result,
		{
			src: "https://via.placeholder.com/450x450",
			alt: "fuji",
			class: "img-responsive"
		},
		{},
		{}
	)}</div>
                <div class="${"column col-6"}">${validate_component(Image, "Image").$$render(
		$$result,
		{
			src: "https://via.placeholder.com/450x450",
			alt: "fuji",
			class: "img-responsive"
		},
		{},
		{}
	)}</div>
                <div class="${"column col-6"}">${validate_component(Image, "Image").$$render(
		$$result,
		{
			src: "https://via.placeholder.com/450x450",
			alt: "fuji",
			class: "img-responsive"
		},
		{},
		{}
	)}</div>
                <div class="${"column col-6"}">${validate_component(Image, "Image").$$render(
		$$result,
		{
			src: "https://via.placeholder.com/450x450",
			alt: "fuji",
			class: "img-responsive"
		},
		{},
		{}
	)}</div>
                <div class="${"column col-6"}">${validate_component(Image, "Image").$$render(
		$$result,
		{
			src: "https://via.placeholder.com/450x450",
			alt: "fuji",
			class: "img-responsive"
		},
		{},
		{}
	)}</div></div></div></div></div></div>`;
});

var activity$1 = /*#__PURE__*/Object.freeze({
    __proto__: null,
    'default': Activity
});

/* src/routes/about.svelte generated by Svelte v3.24.0 */

const About = create_ssr_component(($$result, $$props, $$bindings, $$slots) => {
	return `${($$result.head += `${($$result.title = `<title>About</title>`, "")}`, "")}

<h1>About this site</h1>

<p>This is the &#39;about&#39; page. There&#39;s not much here.</p>`;
});

var about = /*#__PURE__*/Object.freeze({
    __proto__: null,
    'default': About
});

/* src/routes/login.svelte generated by Svelte v3.24.0 */

const Login = create_ssr_component(($$result, $$props, $$bindings, $$slots) => {
	
	let email = "";
	let password = "";
	user.logued.set(false);

	return `<nav class="${"panel mt-2"}"><div class="${"panel-header"}">Ingresarás con tu cuenta</div>
	<div class="${"panel-body"}"><div class="${"form-group"}"><label class="${"form-label"}">Correo</label>
			<input class="${"form-input"}" type="${"text"}" name="${"correo"}" placeholder="${"email"}"${add_attribute("value", email, 1)}></div>
		<div class="${"form-group"}"><label class="${"form-label"}">Clave</label>
			<input class="${"form-input"}" type="${"password"}" name="${"password"}" placeholder="${"password"}"${add_attribute("value", password, 1)}></div>
		<button class="${"btn btn-block"}">Continuar</button></div>
	<div class="${"panel-footer"}"><button class="${"btn btn-primary btn-block s-rounded"}">Google</button></div></nav>`;
});

var login = /*#__PURE__*/Object.freeze({
    __proto__: null,
    'default': Login
});

/* src/routes/club.svelte generated by Svelte v3.24.0 */

const css$2 = {
	code: ".panel-nav.svelte-qr3mpb{padding:.8rem}",
	map: "{\"version\":3,\"file\":\"club.svelte\",\"sources\":[\"club.svelte\"],\"sourcesContent\":[\"<!-- https://eugenkiss.github.io/7guis/tasks#crud -->\\n\\n<script>\\n\\timport { club } from '../store.js';\\n</script>\\n\\n<style>\\n\\t.panel-nav{\\n\\t\\tpadding: .8rem;\\n\\t}\\n</style>\\n\\n<div class=\\\"panel\\\" style=\\\"border: 0;\\\">\\n    \\n\\t<div class=\\\"panel-nav\\\">\\n\\t\\t<ul class=\\\"tab tab-block\\\">\\n\\t\\t  <li class=\\\"tab-item active\\\">\\n\\t\\t    <a href=\\\"club\\\" class=\\\"active\\\">Historia</a>\\n\\t\\t  </li>\\n\\t\\t  <li class=\\\"tab-item\\\">\\n\\t\\t    <a href=\\\"club\\\">Instalaciones</a>\\n\\t\\t  </li>\\n\\t\\t  <li class=\\\"tab-item\\\">\\n\\t\\t    <a href=\\\"club\\\">Miembros</a>\\n\\t\\t  </li>\\n\\t\\t  <li class=\\\"tab-item\\\">\\n\\t\\t    <a href=\\\"club\\\">Eventos</a>\\n\\t\\t  </li>\\n\\t\\t</ul>\\n\\t</div>\\n\\n    <div class=\\\"panel-header text-center\\\">\\n      <img src=\\\"{$club.image}\\\" alt=\\\"img\\\" class=\\\"img-responsive\\\">\\n    </div>\\n    \\n    <div class=\\\"panel-body\\\">\\n    \\t<h1>Historia</h1>\\n    \\t<p>{$club.history}</p>\\n\\n\\n\\t  \\t<div>\\n\\t  \\t\\t<figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure><figure class=\\\"avatar avatar-xl\\\"><img src=\\\"{$club.image}\\\" alt=\\\"{$club.name}\\\"></figure>\\n\\t  \\t</div>\\n\\n  \\t</div>\\n</div>\\n\"],\"names\":[],\"mappings\":\"AAOC,wBAAU,CAAC,AACV,OAAO,CAAE,KAAK,AACf,CAAC\"}"
};

const Club = create_ssr_component(($$result, $$props, $$bindings, $$slots) => {
	let $club = get_store_value(club);
	$$result.css.add(css$2);

	return `





<div class="${"panel"}" style="${"border: 0;"}"><div class="${"panel-nav svelte-qr3mpb"}"><ul class="${"tab tab-block"}"><li class="${"tab-item active"}"><a href="${"club"}" class="${"active"}">Historia</a></li>
		  <li class="${"tab-item"}"><a href="${"club"}">Instalaciones</a></li>
		  <li class="${"tab-item"}"><a href="${"club"}">Miembros</a></li>
		  <li class="${"tab-item"}"><a href="${"club"}">Eventos</a></li></ul></div>

    <div class="${"panel-header text-center"}"><img${add_attribute("src", $club.image, 0)} alt="${"img"}" class="${"img-responsive"}"></div>
    
    <div class="${"panel-body"}"><h1>Historia</h1>
    	<p>${escape($club.history)}</p>


	  	<div><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure><figure class="${"avatar avatar-xl"}"><img${add_attribute("src", $club.image, 0)}${add_attribute("alt", $club.name, 0)}></figure></div></div></div>`;
});

var club$1 = /*#__PURE__*/Object.freeze({
    __proto__: null,
    'default': Club
});

/* src/routes/room.svelte generated by Svelte v3.24.0 */

const Room = create_ssr_component(($$result, $$props, $$bindings, $$slots) => {
	let $club = get_store_value(club);

	return `<div class="${"mainroom panel"}"><div class="${"panel-nav"}"><ul class="${"tab tab-block"}"><li class="${"tab-item active"}"><a href="${"room"}">Sala 1</a></li>
      <li class="${"tab-item"}"><a href="${"."}">Sala 2</a></li></ul></div>

  <div class="${"panel-header"}"><div class="${"panel-title h6"}">Sala 1</div></div>

  <div class="${"panel-body"}" style="${"overflow-x: scroll;"}"><div class="${"tile"}"><div class="${"tile-icon"}"><figure class="${"avatar"}"><img src="${"https://via.placeholder.com/150"}" alt="${"Avatar"}"></figure></div>
      <div class="${"tile-content"}"><p class="${"tile-title text-bold"}">Pablo Perez</p>
        <p class="${"tile-subtitle"}">Que lindo volver a jugarr!!!!!</p></div></div>

    <div class="${"tile"}"><div class="${"tile-icon"}"><figure class="${"avatar"}"><img src="${"https://via.placeholder.com/150"}" alt="${"Avatar"}"></figure></div>
      <div class="${"tile-content"}"><p class="${"tile-title text-bold"}">Nacho</p>
        <p class="${"tile-subtitle"}">Queremos verte otra ver</p>
        <img src="${"https://via.placeholder.com/450"}" alt="${"Avatar"}" class="${"img-responsive"}"></div></div>

      
      <div class="${"tile tile-centered tile-partido"}"><div class="${"tile-partido-info"}"><small>01-01-2020 en cancha principal</small></div>
        <div class="${"tile-content"}"><div class="${"tile-title"}">CAS <strong>2</strong></div>
          <figure class="${"avatar avatar-lg"}"><img${add_attribute("src", $club.image, 0)} alt="${"Avatar"}"></figure>
          <figure class="${"avatar avatar-lg"}"><img${add_attribute("src", $club.image, 0)} alt="${"Avatar"}"></figure>
          <div class="${"tile-title"}"><strong>0</strong> A.V.L.</div></div></div></div>
  <div class="${"panel-footer"}"><div class="${"input-group"}"><input class="${"form-input"}" type="${"text"}" placeholder="${"Hello"}">
      <button class="${"btn btn-primary input-group-btn"}">Send</button></div></div></div>`;
});

var room = /*#__PURE__*/Object.freeze({
    __proto__: null,
    'default': Room
});

/* src/routes/user.svelte generated by Svelte v3.24.0 */

const User = create_ssr_component(($$result, $$props, $$bindings, $$slots) => {
	let $club = get_store_value(club);

	return `<nav class="${"mainroom panel"}"><div class="${"panel-header text-center"}"><figure class="${"avatar avatar-lg"}"><img${add_attribute("src", $club.image, 0)} alt="${"Avatar"}"></figure>
      <div class="${"panel-title h5 mt-10"}">Pablo Perez</div>
      <div class="${"chip"}"><i class="${"icon icon-message"}"></i></div>
      <div class="${"chip"}"><i class="${"icon icon-edit"}"></i></div></div>

	<div class="${"panel-body"}"><div class="${"tile"}"><div class="${"tile-title h2"}">Clubes</div></div>
		<div class="${"columns"}"><div class="${"column col-auto"}"><label>${escape($club.name)}</label>
				<a href="${"."}"><figure class="${"avatar avatar-xl"}"><img src="${"https://scontent.faep8-1.fna.fbcdn.net/v/t1.0-9/15781504_1196905870358249_4023066058224689613_n.png?_nc_cat=111&_nc_sid=09cbfe&_nc_oc=AQl_i8CDXAgN9ufKJJMUh17KPHxrTmDClhliURH1uePKtaiI6AuthjmbfR1oZMn_U-Q&_nc_ht=scontent.faep8-1.fna&oh=7d4443ec7bfb06977a507209b77e9b4a&oe=5F2CEC1D"}" alt="${"Avatar"}" class="${"img-reponsive"}"></figure></a></div></div></div></nav>`;
});

var user$1 = /*#__PURE__*/Object.freeze({
    __proto__: null,
    'default': User
});

/* src/components/Nav.svelte generated by Svelte v3.24.0 */

const css$3 = {
	code: ".menu.svelte-1sg4mug{background:none;box-shadow:none}",
	map: "{\"version\":3,\"file\":\"Nav.svelte\",\"sources\":[\"Nav.svelte\"],\"sourcesContent\":[\"<script>\\n  import { club } from '../store.js';\\n  export let segment; \\n</script>\\n\\n\\n  <div id=\\\"sidebar-id\\\" class=\\\"off-canvas-sidebar\\\">\\n    <ul class=\\\"menu\\\">\\n      <li class=\\\"menu-item\\\">\\n        <a href=\\\".\\\">{$club.alias}</a>\\n      </li>\\n      <li class=\\\"menu-item\\\">\\n        <a href=\\\"club\\\">Identidad</a>\\n      </li>\\n      <li class=\\\"menu-item\\\">\\n        <a href=\\\"activity\\\">Actividades</a>\\n      </li>\\n      <li class=\\\"menu-item\\\">\\n        <a href=\\\"user\\\">Usuario</a>\\n      </li>\\n      <li class=\\\"menu-item\\\">\\n        <a href=\\\"login\\\">Login</a>\\n      </li>\\n      <li class=\\\"menu-item\\\">\\n        <a href=\\\"room\\\">Salas</a>\\n      </li>\\n\\n    </ul>\\n\\n  </div>\\n\\n  <a class=\\\"off-canvas-overlay\\\" href=\\\"{(segment!=undefined) ? segment : ''}#close\\\">\\n    <input type=\\\"hidden\\\" name=\\\"hidden\\\">\\n  </a>\\n\\n\\n<style>\\n  .menu{\\n    background: none;\\n    box-shadow: none;\\n  }\\n</style>\"],\"names\":[],\"mappings\":\"AAqCE,oBAAK,CAAC,AACJ,UAAU,CAAE,IAAI,CAChB,UAAU,CAAE,IAAI,AAClB,CAAC\"}"
};

const Nav = create_ssr_component(($$result, $$props, $$bindings, $$slots) => {
	let $club = get_store_value(club);
	let { segment } = $$props;
	if ($$props.segment === void 0 && $$bindings.segment && segment !== void 0) $$bindings.segment(segment);
	$$result.css.add(css$3);

	return `<div id="${"sidebar-id"}" class="${"off-canvas-sidebar"}"><ul class="${"menu svelte-1sg4mug"}"><li class="${"menu-item"}"><a href="${"."}">${escape($club.alias)}</a></li>
      <li class="${"menu-item"}"><a href="${"club"}">Identidad</a></li>
      <li class="${"menu-item"}"><a href="${"activity"}">Actividades</a></li>
      <li class="${"menu-item"}"><a href="${"user"}">Usuario</a></li>
      <li class="${"menu-item"}"><a href="${"login"}">Login</a></li>
      <li class="${"menu-item"}"><a href="${"room"}">Salas</a></li></ul></div>

  <a class="${"off-canvas-overlay"}" href="${escape(segment != undefined ? segment : "") + "#close"}"><input type="${"hidden"}" name="${"hidden"}">
  </a>`;
});

const CONTEXT_KEY = {};

const preload = () => ({});

/* src/routes/_error.svelte generated by Svelte v3.24.0 */

const css$4 = {
	code: "h1.svelte-8od9u6,p.svelte-8od9u6{margin:0 auto}h1.svelte-8od9u6{font-size:2.8em;font-weight:700;margin:0 0 0.5em 0}p.svelte-8od9u6{margin:1em auto}@media(min-width: 480px){h1.svelte-8od9u6{font-size:4em}}",
	map: "{\"version\":3,\"file\":\"_error.svelte\",\"sources\":[\"_error.svelte\"],\"sourcesContent\":[\"<script>\\n\\texport let status;\\n\\texport let error;\\n\\n\\tconst dev = undefined === 'development';\\n</script>\\n\\n<style>\\n\\th1, p {\\n\\t\\tmargin: 0 auto;\\n\\t}\\n\\n\\th1 {\\n\\t\\tfont-size: 2.8em;\\n\\t\\tfont-weight: 700;\\n\\t\\tmargin: 0 0 0.5em 0;\\n\\t}\\n\\n\\tp {\\n\\t\\tmargin: 1em auto;\\n\\t}\\n\\n\\t@media (min-width: 480px) {\\n\\t\\th1 {\\n\\t\\t\\tfont-size: 4em;\\n\\t\\t}\\n\\t}\\n</style>\\n\\n<svelte:head>\\n\\t<title>{status}</title>\\n</svelte:head>\\n\\n<h1>{status}</h1>\\n\\n<p>{error.message}</p>\\n\\n{#if dev && error.stack}\\n\\t<pre>{error.stack}</pre>\\n{/if}\\n\"],\"names\":[],\"mappings\":\"AAQC,gBAAE,CAAE,CAAC,cAAC,CAAC,AACN,MAAM,CAAE,CAAC,CAAC,IAAI,AACf,CAAC,AAED,EAAE,cAAC,CAAC,AACH,SAAS,CAAE,KAAK,CAChB,WAAW,CAAE,GAAG,CAChB,MAAM,CAAE,CAAC,CAAC,CAAC,CAAC,KAAK,CAAC,CAAC,AACpB,CAAC,AAED,CAAC,cAAC,CAAC,AACF,MAAM,CAAE,GAAG,CAAC,IAAI,AACjB,CAAC,AAED,MAAM,AAAC,YAAY,KAAK,CAAC,AAAC,CAAC,AAC1B,EAAE,cAAC,CAAC,AACH,SAAS,CAAE,GAAG,AACf,CAAC,AACF,CAAC\"}"
};

const Error$1 = create_ssr_component(($$result, $$props, $$bindings, $$slots) => {
	let { status } = $$props;
	let { error } = $$props;
	if ($$props.status === void 0 && $$bindings.status && status !== void 0) $$bindings.status(status);
	if ($$props.error === void 0 && $$bindings.error && error !== void 0) $$bindings.error(error);
	$$result.css.add(css$4);

	return `${($$result.head += `${($$result.title = `<title>${escape(status)}</title>`, "")}`, "")}

<h1 class="${"svelte-8od9u6"}">${escape(status)}</h1>

<p class="${"svelte-8od9u6"}">${escape(error.message)}</p>

${ ``}`;
});

/* src/node_modules/@sapper/internal/App.svelte generated by Svelte v3.24.0 */

const App = create_ssr_component(($$result, $$props, $$bindings, $$slots) => {
	let { stores } = $$props;
	let { error } = $$props;
	let { status } = $$props;
	let { segments } = $$props;
	let { level0 } = $$props;
	let { level1 = null } = $$props;
	let { notify } = $$props;
	afterUpdate(notify);
	setContext(CONTEXT_KEY, stores);
	if ($$props.stores === void 0 && $$bindings.stores && stores !== void 0) $$bindings.stores(stores);
	if ($$props.error === void 0 && $$bindings.error && error !== void 0) $$bindings.error(error);
	if ($$props.status === void 0 && $$bindings.status && status !== void 0) $$bindings.status(status);
	if ($$props.segments === void 0 && $$bindings.segments && segments !== void 0) $$bindings.segments(segments);
	if ($$props.level0 === void 0 && $$bindings.level0 && level0 !== void 0) $$bindings.level0(level0);
	if ($$props.level1 === void 0 && $$bindings.level1 && level1 !== void 0) $$bindings.level1(level1);
	if ($$props.notify === void 0 && $$bindings.notify && notify !== void 0) $$bindings.notify(notify);

	return `


${validate_component(Layout, "Layout").$$render($$result, Object.assign({ segment: segments[0] }, level0.props), {}, {
		default: () => `${error
		? `${validate_component(Error$1, "Error").$$render($$result, { error, status }, {}, {})}`
		: `${validate_component(level1.component || missing_component, "svelte:component").$$render($$result, Object.assign(level1.props), {}, {})}`}`
	})}`;
});

// This file is generated by Sapper — do not edit it!

const ignore = [];

const components = [
	{
		js: () => Promise.resolve().then(function () { return index; }),
		css: "__SAPPER_CSS_PLACEHOLDER:index.svelte__"
	},
	{
		js: () => Promise.resolve().then(function () { return activity$1; }),
		css: "__SAPPER_CSS_PLACEHOLDER:activity.svelte__"
	},
	{
		js: () => Promise.resolve().then(function () { return about; }),
		css: "__SAPPER_CSS_PLACEHOLDER:about.svelte__"
	},
	{
		js: () => Promise.resolve().then(function () { return login; }),
		css: "__SAPPER_CSS_PLACEHOLDER:login.svelte__"
	},
	{
		js: () => Promise.resolve().then(function () { return club$1; }),
		css: "__SAPPER_CSS_PLACEHOLDER:club.svelte__"
	},
	{
		js: () => Promise.resolve().then(function () { return room; }),
		css: "__SAPPER_CSS_PLACEHOLDER:room.svelte__"
	},
	{
		js: () => Promise.resolve().then(function () { return user$1; }),
		css: "__SAPPER_CSS_PLACEHOLDER:user.svelte__"
	}
];

const routes = [
	{
		// index.svelte
		pattern: /^\/$/,
		parts: [
			{ i: 0 }
		]
	},

	{
		// activity.svelte
		pattern: /^\/activity\/?$/,
		parts: [
			{ i: 1 }
		]
	},

	{
		// about.svelte
		pattern: /^\/about\/?$/,
		parts: [
			{ i: 2 }
		]
	},

	{
		// login.svelte
		pattern: /^\/login\/?$/,
		parts: [
			{ i: 3 }
		]
	},

	{
		// club.svelte
		pattern: /^\/club\/?$/,
		parts: [
			{ i: 4 }
		]
	},

	{
		// room.svelte
		pattern: /^\/room\/?$/,
		parts: [
			{ i: 5 }
		]
	},

	{
		// user.svelte
		pattern: /^\/user\/?$/,
		parts: [
			{ i: 6 }
		]
	}
];

function goto(href, opts = { replaceState: false }) {
	const target = select_target(new URL(href, document.baseURI));

	if (target) {
		_history[opts.replaceState ? 'replaceState' : 'pushState']({ id: cid }, '', href);
		return navigate(target, null).then(() => {});
	}

	location.href = href;
	return new Promise(f => {}); // never resolves
}

/** Callback to inform of a value updates. */



















function page_store(value) {
	const store = writable(value);
	let ready = true;

	function notify() {
		ready = true;
		store.update(val => val);
	}

	function set(new_value) {
		ready = false;
		store.set(new_value);
	}

	function subscribe(run) {
		let old_value;
		return store.subscribe((value) => {
			if (old_value === undefined || (ready && value !== old_value)) {
				run(old_value = value);
			}
		});
	}

	return { notify, set, subscribe };
}

const initial_data = typeof __SAPPER__ !== 'undefined' && __SAPPER__;

let ready = false;
let root_component;
let current_token;
let root_preloaded;
let current_branch = [];
let current_query = '{}';

const stores = {
	page: page_store({}),
	preloading: writable(null),
	session: writable(initial_data && initial_data.session)
};

let $session;
let session_dirty;

stores.session.subscribe(async value => {
	$session = value;

	if (!ready) return;
	session_dirty = true;

	const target = select_target(new URL(location.href));

	const token = current_token = {};
	const { redirect, props, branch } = await hydrate_target(target);
	if (token !== current_token) return; // a secondary navigation happened while we were loading

	await render(redirect, branch, props, target.page);
});

let prefetching


 = null;

let target;

let uid = 1;

let cid;

const _history = typeof history !== 'undefined' ? history : {
	pushState: (state, title, href) => {},
	replaceState: (state, title, href) => {},
	scrollRestoration: ''
};

const scroll_history = {};

function extract_query(search) {
	const query = Object.create(null);
	if (search.length > 0) {
		search.slice(1).split('&').forEach(searchParam => {
			let [, key, value = ''] = /([^=]*)(?:=(.*))?/.exec(decodeURIComponent(searchParam.replace(/\+/g, ' ')));
			if (typeof query[key] === 'string') query[key] = [query[key]];
			if (typeof query[key] === 'object') (query[key] ).push(value);
			else query[key] = value;
		});
	}
	return query;
}

function select_target(url) {
	if (url.origin !== location.origin) return null;
	if (!url.pathname.startsWith(initial_data.baseUrl)) return null;

	let path = url.pathname.slice(initial_data.baseUrl.length);

	if (path === '') {
		path = '/';
	}

	// avoid accidental clashes between server routes and page routes
	if (ignore.some(pattern => pattern.test(path))) return;

	for (let i = 0; i < routes.length; i += 1) {
		const route = routes[i];

		const match = route.pattern.exec(path);

		if (match) {
			const query = extract_query(url.search);
			const part = route.parts[route.parts.length - 1];
			const params = part.params ? part.params(match) : {};

			const page = { host: location.host, path, query, params };

			return { href: url.href, route, match, page };
		}
	}
}

function scroll_state() {
	return {
		x: pageXOffset,
		y: pageYOffset
	};
}

async function navigate(target, id, noscroll, hash) {
	if (id) {
		// popstate or initial navigation
		cid = id;
	} else {
		const current_scroll = scroll_state();

		// clicked on a link. preserve scroll state
		scroll_history[cid] = current_scroll;

		id = cid = ++uid;
		scroll_history[cid] = noscroll ? current_scroll : { x: 0, y: 0 };
	}

	cid = id;

	if (root_component) stores.preloading.set(true);

	const loaded = prefetching && prefetching.href === target.href ?
		prefetching.promise :
		hydrate_target(target);

	prefetching = null;

	const token = current_token = {};
	const { redirect, props, branch } = await loaded;
	if (token !== current_token) return; // a secondary navigation happened while we were loading

	await render(redirect, branch, props, target.page);
	if (document.activeElement) document.activeElement.blur();

	if (!noscroll) {
		let scroll = scroll_history[id];

		if (hash) {
			// scroll is an element id (from a hash), we need to compute y.
			const deep_linked = document.getElementById(hash.slice(1));

			if (deep_linked) {
				scroll = {
					x: 0,
					y: deep_linked.getBoundingClientRect().top + scrollY
				};
			}
		}

		scroll_history[cid] = scroll;
		if (scroll) scrollTo(scroll.x, scroll.y);
	}
}

async function render(redirect, branch, props, page) {
	if (redirect) return goto(redirect.location, { replaceState: true });

	stores.page.set(page);
	stores.preloading.set(false);

	if (root_component) {
		root_component.$set(props);
	} else {
		props.stores = {
			page: { subscribe: stores.page.subscribe },
			preloading: { subscribe: stores.preloading.subscribe },
			session: stores.session
		};
		props.level0 = {
			props: await root_preloaded
		};
		props.notify = stores.page.notify;

		// first load — remove SSR'd <head> contents
		const start = document.querySelector('#sapper-head-start');
		const end = document.querySelector('#sapper-head-end');

		if (start && end) {
			while (start.nextSibling !== end) detach(start.nextSibling);
			detach(start);
			detach(end);
		}

		root_component = new App({
			target,
			props,
			hydrate: true
		});
	}

	current_branch = branch;
	current_query = JSON.stringify(page.query);
	ready = true;
	session_dirty = false;
}

function part_changed(i, segment, match, stringified_query) {
	// TODO only check query string changes for preload functions
	// that do in fact depend on it (using static analysis or
	// runtime instrumentation)
	if (stringified_query !== current_query) return true;

	const previous = current_branch[i];

	if (!previous) return false;
	if (segment !== previous.segment) return true;
	if (previous.match) {
		if (JSON.stringify(previous.match.slice(1, i + 2)) !== JSON.stringify(match.slice(1, i + 2))) {
			return true;
		}
	}
}

async function hydrate_target(target)



 {
	const { route, page } = target;
	const segments = page.path.split('/').filter(Boolean);

	let redirect = null;

	const props = { error: null, status: 200, segments: [segments[0]] };

	const preload_context = {
		fetch: (url, opts) => fetch(url, opts),
		redirect: (statusCode, location) => {
			if (redirect && (redirect.statusCode !== statusCode || redirect.location !== location)) {
				throw new Error(`Conflicting redirects`);
			}
			redirect = { statusCode, location };
		},
		error: (status, error) => {
			props.error = typeof error === 'string' ? new Error(error) : error;
			props.status = status;
		}
	};

	if (!root_preloaded) {
		root_preloaded = initial_data.preloaded[0] || preload.call(preload_context, {
			host: page.host,
			path: page.path,
			query: page.query,
			params: {}
		}, $session);
	}

	let branch;
	let l = 1;

	try {
		const stringified_query = JSON.stringify(page.query);
		const match = route.pattern.exec(page.path);

		let segment_dirty = false;

		branch = await Promise.all(route.parts.map(async (part, i) => {
			const segment = segments[i];

			if (part_changed(i, segment, match, stringified_query)) segment_dirty = true;

			props.segments[l] = segments[i + 1]; // TODO make this less confusing
			if (!part) return { segment };

			const j = l++;

			if (!session_dirty && !segment_dirty && current_branch[i] && current_branch[i].part === part.i) {
				return current_branch[i];
			}

			segment_dirty = false;

			const { default: component, preload } = await load_component(components[part.i]);

			let preloaded;
			if (ready || !initial_data.preloaded[i + 1]) {
				preloaded = preload
					? await preload.call(preload_context, {
						host: page.host,
						path: page.path,
						query: page.query,
						params: part.params ? part.params(target.match) : {}
					}, $session)
					: {};
			} else {
				preloaded = initial_data.preloaded[i + 1];
			}

			return (props[`level${j}`] = { component, props: preloaded, segment, match, part: part.i });
		}));
	} catch (error) {
		props.error = error;
		props.status = 500;
		branch = [];
	}

	return { redirect, props, branch };
}

function load_css(chunk) {
	const href = `client/${chunk}`;
	if (document.querySelector(`link[href="${href}"]`)) return;

	return new Promise((fulfil, reject) => {
		const link = document.createElement('link');
		link.rel = 'stylesheet';
		link.href = href;

		link.onload = () => fulfil();
		link.onerror = reject;

		document.head.appendChild(link);
	});
}

function load_component(component)


 {
	// TODO this is temporary — once placeholders are
	// always rewritten, scratch the ternary
	const promises = (typeof component.css === 'string' ? [] : component.css.map(load_css));
	promises.unshift(component.js());
	return Promise.all(promises).then(values => values[0]);
}

function detach(node) {
	node.parentNode.removeChild(node);
}

/* src/routes/_layout.svelte generated by Svelte v3.24.0 */

const Layout = create_ssr_component(($$result, $$props, $$bindings, $$slots) => {
	let $club = get_store_value(club);
	let login_ = false;

	onMount(() => {
		user.logued.subscribe(async value => {
			login_ = value;
			if (login_) await goto("/user"); else await goto("/login");
		});
	});

	let { segment } = $$props;
	if ($$props.segment === void 0 && $$bindings.segment && segment !== void 0) $$bindings.segment(segment);

	return `<div class="${"off-canvas off-canvas-sidebar-show"}">${login_
	? `${validate_component(Nav, "Nav").$$render($$result, { segment }, {}, {})}`
	: ``}

	<div class="${"off-canvas-content"}"><div class="${"container grid-lg"}"><div class="${"columns"}"><div class="${"main column col-12 col-mx-auto"}">${login_
	? `<header class="${"navbar show-xs"}"><section class="${"navbar-section"}"><a class="${"off-canvas-toggle btn btn-primary btn-action"}" href="${escape(segment != undefined ? segment : "") + "#sidebar-id"}"><i class="${"icon icon-menu"}"></i></a>
						    <a href="${"."}" class="${"btn btn-link"}">${escape($club.alias)}</a></section></header>`
	: ``}
					${$$slots.default ? $$slots.default({}) : ``}</div></div></div></div></div>`;
});

// This file is generated by Sapper — do not edit it!

const manifest = {
	server_routes: [
		
	],

	pages: [
		{
			// index.svelte
			pattern: /^\/$/,
			parts: [
				{ name: "index", file: "index.svelte", component: Routes }
			]
		},

		{
			// activity.svelte
			pattern: /^\/activity\/?$/,
			parts: [
				{ name: "activity", file: "activity.svelte", component: Activity }
			]
		},

		{
			// about.svelte
			pattern: /^\/about\/?$/,
			parts: [
				{ name: "about", file: "about.svelte", component: About }
			]
		},

		{
			// login.svelte
			pattern: /^\/login\/?$/,
			parts: [
				{ name: "login", file: "login.svelte", component: Login }
			]
		},

		{
			// club.svelte
			pattern: /^\/club\/?$/,
			parts: [
				{ name: "club", file: "club.svelte", component: Club }
			]
		},

		{
			// room.svelte
			pattern: /^\/room\/?$/,
			parts: [
				{ name: "room", file: "room.svelte", component: Room }
			]
		},

		{
			// user.svelte
			pattern: /^\/user\/?$/,
			parts: [
				{ name: "user", file: "user.svelte", component: User }
			]
		}
	],

	root: Layout,
	root_preload: () => {},
	error: Error$1
};

const build_dir = "__sapper__/build";

/**
 * @param typeMap [Object] Map of MIME type -> Array[extensions]
 * @param ...
 */
function Mime() {
  this._types = Object.create(null);
  this._extensions = Object.create(null);

  for (var i = 0; i < arguments.length; i++) {
    this.define(arguments[i]);
  }

  this.define = this.define.bind(this);
  this.getType = this.getType.bind(this);
  this.getExtension = this.getExtension.bind(this);
}

/**
 * Define mimetype -> extension mappings.  Each key is a mime-type that maps
 * to an array of extensions associated with the type.  The first extension is
 * used as the default extension for the type.
 *
 * e.g. mime.define({'audio/ogg', ['oga', 'ogg', 'spx']});
 *
 * If a type declares an extension that has already been defined, an error will
 * be thrown.  To suppress this error and force the extension to be associated
 * with the new type, pass `force`=true.  Alternatively, you may prefix the
 * extension with "*" to map the type to extension, without mapping the
 * extension to the type.
 *
 * e.g. mime.define({'audio/wav', ['wav']}, {'audio/x-wav', ['*wav']});
 *
 *
 * @param map (Object) type definitions
 * @param force (Boolean) if true, force overriding of existing definitions
 */
Mime.prototype.define = function(typeMap, force) {
  for (var type in typeMap) {
    var extensions = typeMap[type].map(function(t) {return t.toLowerCase()});
    type = type.toLowerCase();

    for (var i = 0; i < extensions.length; i++) {
      var ext = extensions[i];

      // '*' prefix = not the preferred type for this extension.  So fixup the
      // extension, and skip it.
      if (ext[0] == '*') {
        continue;
      }

      if (!force && (ext in this._types)) {
        throw new Error(
          'Attempt to change mapping for "' + ext +
          '" extension from "' + this._types[ext] + '" to "' + type +
          '". Pass `force=true` to allow this, otherwise remove "' + ext +
          '" from the list of extensions for "' + type + '".'
        );
      }

      this._types[ext] = type;
    }

    // Use first extension as default
    if (force || !this._extensions[type]) {
      var ext = extensions[0];
      this._extensions[type] = (ext[0] != '*') ? ext : ext.substr(1);
    }
  }
};

/**
 * Lookup a mime type based on extension
 */
Mime.prototype.getType = function(path) {
  path = String(path);
  var last = path.replace(/^.*[/\\]/, '').toLowerCase();
  var ext = last.replace(/^.*\./, '').toLowerCase();

  var hasPath = last.length < path.length;
  var hasDot = ext.length < last.length - 1;

  return (hasDot || !hasPath) && this._types[ext] || null;
};

/**
 * Return file extension associated with a mime type
 */
Mime.prototype.getExtension = function(type) {
  type = /^\s*([^;\s]*)/.test(type) && RegExp.$1;
  return type && this._extensions[type.toLowerCase()] || null;
};

var Mime_1 = Mime;

var standard = {"application/andrew-inset":["ez"],"application/applixware":["aw"],"application/atom+xml":["atom"],"application/atomcat+xml":["atomcat"],"application/atomsvc+xml":["atomsvc"],"application/bdoc":["bdoc"],"application/ccxml+xml":["ccxml"],"application/cdmi-capability":["cdmia"],"application/cdmi-container":["cdmic"],"application/cdmi-domain":["cdmid"],"application/cdmi-object":["cdmio"],"application/cdmi-queue":["cdmiq"],"application/cu-seeme":["cu"],"application/dash+xml":["mpd"],"application/davmount+xml":["davmount"],"application/docbook+xml":["dbk"],"application/dssc+der":["dssc"],"application/dssc+xml":["xdssc"],"application/ecmascript":["ecma","es"],"application/emma+xml":["emma"],"application/epub+zip":["epub"],"application/exi":["exi"],"application/font-tdpfr":["pfr"],"application/geo+json":["geojson"],"application/gml+xml":["gml"],"application/gpx+xml":["gpx"],"application/gxf":["gxf"],"application/gzip":["gz"],"application/hjson":["hjson"],"application/hyperstudio":["stk"],"application/inkml+xml":["ink","inkml"],"application/ipfix":["ipfix"],"application/java-archive":["jar","war","ear"],"application/java-serialized-object":["ser"],"application/java-vm":["class"],"application/javascript":["js","mjs"],"application/json":["json","map"],"application/json5":["json5"],"application/jsonml+json":["jsonml"],"application/ld+json":["jsonld"],"application/lost+xml":["lostxml"],"application/mac-binhex40":["hqx"],"application/mac-compactpro":["cpt"],"application/mads+xml":["mads"],"application/manifest+json":["webmanifest"],"application/marc":["mrc"],"application/marcxml+xml":["mrcx"],"application/mathematica":["ma","nb","mb"],"application/mathml+xml":["mathml"],"application/mbox":["mbox"],"application/mediaservercontrol+xml":["mscml"],"application/metalink+xml":["metalink"],"application/metalink4+xml":["meta4"],"application/mets+xml":["mets"],"application/mods+xml":["mods"],"application/mp21":["m21","mp21"],"application/mp4":["mp4s","m4p"],"application/msword":["doc","dot"],"application/mxf":["mxf"],"application/n-quads":["nq"],"application/n-triples":["nt"],"application/octet-stream":["bin","dms","lrf","mar","so","dist","distz","pkg","bpk","dump","elc","deploy","exe","dll","deb","dmg","iso","img","msi","msp","msm","buffer"],"application/oda":["oda"],"application/oebps-package+xml":["opf"],"application/ogg":["ogx"],"application/omdoc+xml":["omdoc"],"application/onenote":["onetoc","onetoc2","onetmp","onepkg"],"application/oxps":["oxps"],"application/patch-ops-error+xml":["xer"],"application/pdf":["pdf"],"application/pgp-encrypted":["pgp"],"application/pgp-signature":["asc","sig"],"application/pics-rules":["prf"],"application/pkcs10":["p10"],"application/pkcs7-mime":["p7m","p7c"],"application/pkcs7-signature":["p7s"],"application/pkcs8":["p8"],"application/pkix-attr-cert":["ac"],"application/pkix-cert":["cer"],"application/pkix-crl":["crl"],"application/pkix-pkipath":["pkipath"],"application/pkixcmp":["pki"],"application/pls+xml":["pls"],"application/postscript":["ai","eps","ps"],"application/pskc+xml":["pskcxml"],"application/raml+yaml":["raml"],"application/rdf+xml":["rdf","owl"],"application/reginfo+xml":["rif"],"application/relax-ng-compact-syntax":["rnc"],"application/resource-lists+xml":["rl"],"application/resource-lists-diff+xml":["rld"],"application/rls-services+xml":["rs"],"application/rpki-ghostbusters":["gbr"],"application/rpki-manifest":["mft"],"application/rpki-roa":["roa"],"application/rsd+xml":["rsd"],"application/rss+xml":["rss"],"application/rtf":["rtf"],"application/sbml+xml":["sbml"],"application/scvp-cv-request":["scq"],"application/scvp-cv-response":["scs"],"application/scvp-vp-request":["spq"],"application/scvp-vp-response":["spp"],"application/sdp":["sdp"],"application/set-payment-initiation":["setpay"],"application/set-registration-initiation":["setreg"],"application/shf+xml":["shf"],"application/sieve":["siv","sieve"],"application/smil+xml":["smi","smil"],"application/sparql-query":["rq"],"application/sparql-results+xml":["srx"],"application/srgs":["gram"],"application/srgs+xml":["grxml"],"application/sru+xml":["sru"],"application/ssdl+xml":["ssdl"],"application/ssml+xml":["ssml"],"application/tei+xml":["tei","teicorpus"],"application/thraud+xml":["tfi"],"application/timestamped-data":["tsd"],"application/voicexml+xml":["vxml"],"application/wasm":["wasm"],"application/widget":["wgt"],"application/winhlp":["hlp"],"application/wsdl+xml":["wsdl"],"application/wspolicy+xml":["wspolicy"],"application/xaml+xml":["xaml"],"application/xcap-diff+xml":["xdf"],"application/xenc+xml":["xenc"],"application/xhtml+xml":["xhtml","xht"],"application/xml":["xml","xsl","xsd","rng"],"application/xml-dtd":["dtd"],"application/xop+xml":["xop"],"application/xproc+xml":["xpl"],"application/xslt+xml":["xslt"],"application/xspf+xml":["xspf"],"application/xv+xml":["mxml","xhvml","xvml","xvm"],"application/yang":["yang"],"application/yin+xml":["yin"],"application/zip":["zip"],"audio/3gpp":["*3gpp"],"audio/adpcm":["adp"],"audio/basic":["au","snd"],"audio/midi":["mid","midi","kar","rmi"],"audio/mp3":["*mp3"],"audio/mp4":["m4a","mp4a"],"audio/mpeg":["mpga","mp2","mp2a","mp3","m2a","m3a"],"audio/ogg":["oga","ogg","spx"],"audio/s3m":["s3m"],"audio/silk":["sil"],"audio/wav":["wav"],"audio/wave":["*wav"],"audio/webm":["weba"],"audio/xm":["xm"],"font/collection":["ttc"],"font/otf":["otf"],"font/ttf":["ttf"],"font/woff":["woff"],"font/woff2":["woff2"],"image/aces":["exr"],"image/apng":["apng"],"image/bmp":["bmp"],"image/cgm":["cgm"],"image/dicom-rle":["drle"],"image/emf":["emf"],"image/fits":["fits"],"image/g3fax":["g3"],"image/gif":["gif"],"image/heic":["heic"],"image/heic-sequence":["heics"],"image/heif":["heif"],"image/heif-sequence":["heifs"],"image/ief":["ief"],"image/jls":["jls"],"image/jp2":["jp2","jpg2"],"image/jpeg":["jpeg","jpg","jpe"],"image/jpm":["jpm"],"image/jpx":["jpx","jpf"],"image/jxr":["jxr"],"image/ktx":["ktx"],"image/png":["png"],"image/sgi":["sgi"],"image/svg+xml":["svg","svgz"],"image/t38":["t38"],"image/tiff":["tif","tiff"],"image/tiff-fx":["tfx"],"image/webp":["webp"],"image/wmf":["wmf"],"message/disposition-notification":["disposition-notification"],"message/global":["u8msg"],"message/global-delivery-status":["u8dsn"],"message/global-disposition-notification":["u8mdn"],"message/global-headers":["u8hdr"],"message/rfc822":["eml","mime"],"model/3mf":["3mf"],"model/gltf+json":["gltf"],"model/gltf-binary":["glb"],"model/iges":["igs","iges"],"model/mesh":["msh","mesh","silo"],"model/stl":["stl"],"model/vrml":["wrl","vrml"],"model/x3d+binary":["*x3db","x3dbz"],"model/x3d+fastinfoset":["x3db"],"model/x3d+vrml":["*x3dv","x3dvz"],"model/x3d+xml":["x3d","x3dz"],"model/x3d-vrml":["x3dv"],"text/cache-manifest":["appcache","manifest"],"text/calendar":["ics","ifb"],"text/coffeescript":["coffee","litcoffee"],"text/css":["css"],"text/csv":["csv"],"text/html":["html","htm","shtml"],"text/jade":["jade"],"text/jsx":["jsx"],"text/less":["less"],"text/markdown":["markdown","md"],"text/mathml":["mml"],"text/mdx":["mdx"],"text/n3":["n3"],"text/plain":["txt","text","conf","def","list","log","in","ini"],"text/richtext":["rtx"],"text/rtf":["*rtf"],"text/sgml":["sgml","sgm"],"text/shex":["shex"],"text/slim":["slim","slm"],"text/stylus":["stylus","styl"],"text/tab-separated-values":["tsv"],"text/troff":["t","tr","roff","man","me","ms"],"text/turtle":["ttl"],"text/uri-list":["uri","uris","urls"],"text/vcard":["vcard"],"text/vtt":["vtt"],"text/xml":["*xml"],"text/yaml":["yaml","yml"],"video/3gpp":["3gp","3gpp"],"video/3gpp2":["3g2"],"video/h261":["h261"],"video/h263":["h263"],"video/h264":["h264"],"video/jpeg":["jpgv"],"video/jpm":["*jpm","jpgm"],"video/mj2":["mj2","mjp2"],"video/mp2t":["ts"],"video/mp4":["mp4","mp4v","mpg4"],"video/mpeg":["mpeg","mpg","mpe","m1v","m2v"],"video/ogg":["ogv"],"video/quicktime":["qt","mov"],"video/webm":["webm"]};

var lite = new Mime_1(standard);

function get_server_route_handler(routes) {
	async function handle_route(route, req, res, next) {
		req.params = route.params(route.pattern.exec(req.path));

		const method = req.method.toLowerCase();
		// 'delete' cannot be exported from a module because it is a keyword,
		// so check for 'del' instead
		const method_export = method === 'delete' ? 'del' : method;
		const handle_method = route.handlers[method_export];
		if (handle_method) {
			if (process.env.SAPPER_EXPORT) {
				const { write, end, setHeader } = res;
				const chunks = [];
				const headers = {};

				// intercept data so that it can be exported
				res.write = function(chunk) {
					chunks.push(Buffer.from(chunk));
					write.apply(res, arguments);
				};

				res.setHeader = function(name, value) {
					headers[name.toLowerCase()] = value;
					setHeader.apply(res, arguments);
				};

				res.end = function(chunk) {
					if (chunk) chunks.push(Buffer.from(chunk));
					end.apply(res, arguments);

					process.send({
						__sapper__: true,
						event: 'file',
						url: req.url,
						method: req.method,
						status: res.statusCode,
						type: headers['content-type'],
						body: Buffer.concat(chunks).toString()
					});
				};
			}

			const handle_next = (err) => {
				if (err) {
					res.statusCode = 500;
					res.end(err.message);
				} else {
					process.nextTick(next);
				}
			};

			try {
				await handle_method(req, res, handle_next);
			} catch (err) {
				console.error(err);
				handle_next(err);
			}
		} else {
			// no matching handler for method
			process.nextTick(next);
		}
	}

	return function find_route(req, res, next) {
		for (const route of routes) {
			if (route.pattern.test(req.path)) {
				handle_route(route, req, res, next);
				return;
			}
		}

		next();
	};
}

/*!
 * cookie
 * Copyright(c) 2012-2014 Roman Shtylman
 * Copyright(c) 2015 Douglas Christopher Wilson
 * MIT Licensed
 */

/**
 * Module exports.
 * @public
 */

var parse_1 = parse;
var serialize_1 = serialize;

/**
 * Module variables.
 * @private
 */

var decode = decodeURIComponent;
var encode = encodeURIComponent;
var pairSplitRegExp = /; */;

/**
 * RegExp to match field-content in RFC 7230 sec 3.2
 *
 * field-content = field-vchar [ 1*( SP / HTAB ) field-vchar ]
 * field-vchar   = VCHAR / obs-text
 * obs-text      = %x80-FF
 */

var fieldContentRegExp = /^[\u0009\u0020-\u007e\u0080-\u00ff]+$/;

/**
 * Parse a cookie header.
 *
 * Parse the given cookie header string into an object
 * The object has the various cookies as keys(names) => values
 *
 * @param {string} str
 * @param {object} [options]
 * @return {object}
 * @public
 */

function parse(str, options) {
  if (typeof str !== 'string') {
    throw new TypeError('argument str must be a string');
  }

  var obj = {};
  var opt = options || {};
  var pairs = str.split(pairSplitRegExp);
  var dec = opt.decode || decode;

  for (var i = 0; i < pairs.length; i++) {
    var pair = pairs[i];
    var eq_idx = pair.indexOf('=');

    // skip things that don't look like key=value
    if (eq_idx < 0) {
      continue;
    }

    var key = pair.substr(0, eq_idx).trim();
    var val = pair.substr(++eq_idx, pair.length).trim();

    // quoted values
    if ('"' == val[0]) {
      val = val.slice(1, -1);
    }

    // only assign once
    if (undefined == obj[key]) {
      obj[key] = tryDecode(val, dec);
    }
  }

  return obj;
}

/**
 * Serialize data into a cookie header.
 *
 * Serialize the a name value pair into a cookie string suitable for
 * http headers. An optional options object specified cookie parameters.
 *
 * serialize('foo', 'bar', { httpOnly: true })
 *   => "foo=bar; httpOnly"
 *
 * @param {string} name
 * @param {string} val
 * @param {object} [options]
 * @return {string}
 * @public
 */

function serialize(name, val, options) {
  var opt = options || {};
  var enc = opt.encode || encode;

  if (typeof enc !== 'function') {
    throw new TypeError('option encode is invalid');
  }

  if (!fieldContentRegExp.test(name)) {
    throw new TypeError('argument name is invalid');
  }

  var value = enc(val);

  if (value && !fieldContentRegExp.test(value)) {
    throw new TypeError('argument val is invalid');
  }

  var str = name + '=' + value;

  if (null != opt.maxAge) {
    var maxAge = opt.maxAge - 0;
    if (isNaN(maxAge)) throw new Error('maxAge should be a Number');
    str += '; Max-Age=' + Math.floor(maxAge);
  }

  if (opt.domain) {
    if (!fieldContentRegExp.test(opt.domain)) {
      throw new TypeError('option domain is invalid');
    }

    str += '; Domain=' + opt.domain;
  }

  if (opt.path) {
    if (!fieldContentRegExp.test(opt.path)) {
      throw new TypeError('option path is invalid');
    }

    str += '; Path=' + opt.path;
  }

  if (opt.expires) {
    if (typeof opt.expires.toUTCString !== 'function') {
      throw new TypeError('option expires is invalid');
    }

    str += '; Expires=' + opt.expires.toUTCString();
  }

  if (opt.httpOnly) {
    str += '; HttpOnly';
  }

  if (opt.secure) {
    str += '; Secure';
  }

  if (opt.sameSite) {
    var sameSite = typeof opt.sameSite === 'string'
      ? opt.sameSite.toLowerCase() : opt.sameSite;

    switch (sameSite) {
      case true:
        str += '; SameSite=Strict';
        break;
      case 'lax':
        str += '; SameSite=Lax';
        break;
      case 'strict':
        str += '; SameSite=Strict';
        break;
      case 'none':
        str += '; SameSite=None';
        break;
      default:
        throw new TypeError('option sameSite is invalid');
    }
  }

  return str;
}

/**
 * Try decoding a string using a decoding function.
 *
 * @param {string} str
 * @param {function} decode
 * @private
 */

function tryDecode(str, decode) {
  try {
    return decode(str);
  } catch (e) {
    return str;
  }
}

var cookie = {
	parse: parse_1,
	serialize: serialize_1
};

var chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_$';
var unsafeChars = /[<>\b\f\n\r\t\0\u2028\u2029]/g;
var reserved = /^(?:do|if|in|for|int|let|new|try|var|byte|case|char|else|enum|goto|long|this|void|with|await|break|catch|class|const|final|float|short|super|throw|while|yield|delete|double|export|import|native|return|switch|throws|typeof|boolean|default|extends|finally|package|private|abstract|continue|debugger|function|volatile|interface|protected|transient|implements|instanceof|synchronized)$/;
var escaped$1 = {
    '<': '\\u003C',
    '>': '\\u003E',
    '/': '\\u002F',
    '\\': '\\\\',
    '\b': '\\b',
    '\f': '\\f',
    '\n': '\\n',
    '\r': '\\r',
    '\t': '\\t',
    '\0': '\\0',
    '\u2028': '\\u2028',
    '\u2029': '\\u2029'
};
var objectProtoOwnPropertyNames = Object.getOwnPropertyNames(Object.prototype).sort().join('\0');
function devalue(value) {
    var counts = new Map();
    function walk(thing) {
        if (typeof thing === 'function') {
            throw new Error("Cannot stringify a function");
        }
        if (counts.has(thing)) {
            counts.set(thing, counts.get(thing) + 1);
            return;
        }
        counts.set(thing, 1);
        if (!isPrimitive(thing)) {
            var type = getType(thing);
            switch (type) {
                case 'Number':
                case 'String':
                case 'Boolean':
                case 'Date':
                case 'RegExp':
                    return;
                case 'Array':
                    thing.forEach(walk);
                    break;
                case 'Set':
                case 'Map':
                    Array.from(thing).forEach(walk);
                    break;
                default:
                    var proto = Object.getPrototypeOf(thing);
                    if (proto !== Object.prototype &&
                        proto !== null &&
                        Object.getOwnPropertyNames(proto).sort().join('\0') !== objectProtoOwnPropertyNames) {
                        throw new Error("Cannot stringify arbitrary non-POJOs");
                    }
                    if (Object.getOwnPropertySymbols(thing).length > 0) {
                        throw new Error("Cannot stringify POJOs with symbolic keys");
                    }
                    Object.keys(thing).forEach(function (key) { return walk(thing[key]); });
            }
        }
    }
    walk(value);
    var names = new Map();
    Array.from(counts)
        .filter(function (entry) { return entry[1] > 1; })
        .sort(function (a, b) { return b[1] - a[1]; })
        .forEach(function (entry, i) {
        names.set(entry[0], getName(i));
    });
    function stringify(thing) {
        if (names.has(thing)) {
            return names.get(thing);
        }
        if (isPrimitive(thing)) {
            return stringifyPrimitive(thing);
        }
        var type = getType(thing);
        switch (type) {
            case 'Number':
            case 'String':
            case 'Boolean':
                return "Object(" + stringify(thing.valueOf()) + ")";
            case 'RegExp':
                return thing.toString();
            case 'Date':
                return "new Date(" + thing.getTime() + ")";
            case 'Array':
                var members = thing.map(function (v, i) { return i in thing ? stringify(v) : ''; });
                var tail = thing.length === 0 || (thing.length - 1 in thing) ? '' : ',';
                return "[" + members.join(',') + tail + "]";
            case 'Set':
            case 'Map':
                return "new " + type + "([" + Array.from(thing).map(stringify).join(',') + "])";
            default:
                var obj = "{" + Object.keys(thing).map(function (key) { return safeKey(key) + ":" + stringify(thing[key]); }).join(',') + "}";
                var proto = Object.getPrototypeOf(thing);
                if (proto === null) {
                    return Object.keys(thing).length > 0
                        ? "Object.assign(Object.create(null)," + obj + ")"
                        : "Object.create(null)";
                }
                return obj;
        }
    }
    var str = stringify(value);
    if (names.size) {
        var params_1 = [];
        var statements_1 = [];
        var values_1 = [];
        names.forEach(function (name, thing) {
            params_1.push(name);
            if (isPrimitive(thing)) {
                values_1.push(stringifyPrimitive(thing));
                return;
            }
            var type = getType(thing);
            switch (type) {
                case 'Number':
                case 'String':
                case 'Boolean':
                    values_1.push("Object(" + stringify(thing.valueOf()) + ")");
                    break;
                case 'RegExp':
                    values_1.push(thing.toString());
                    break;
                case 'Date':
                    values_1.push("new Date(" + thing.getTime() + ")");
                    break;
                case 'Array':
                    values_1.push("Array(" + thing.length + ")");
                    thing.forEach(function (v, i) {
                        statements_1.push(name + "[" + i + "]=" + stringify(v));
                    });
                    break;
                case 'Set':
                    values_1.push("new Set");
                    statements_1.push(name + "." + Array.from(thing).map(function (v) { return "add(" + stringify(v) + ")"; }).join('.'));
                    break;
                case 'Map':
                    values_1.push("new Map");
                    statements_1.push(name + "." + Array.from(thing).map(function (_a) {
                        var k = _a[0], v = _a[1];
                        return "set(" + stringify(k) + ", " + stringify(v) + ")";
                    }).join('.'));
                    break;
                default:
                    values_1.push(Object.getPrototypeOf(thing) === null ? 'Object.create(null)' : '{}');
                    Object.keys(thing).forEach(function (key) {
                        statements_1.push("" + name + safeProp(key) + "=" + stringify(thing[key]));
                    });
            }
        });
        statements_1.push("return " + str);
        return "(function(" + params_1.join(',') + "){" + statements_1.join(';') + "}(" + values_1.join(',') + "))";
    }
    else {
        return str;
    }
}
function getName(num) {
    var name = '';
    do {
        name = chars[num % chars.length] + name;
        num = ~~(num / chars.length) - 1;
    } while (num >= 0);
    return reserved.test(name) ? name + "_" : name;
}
function isPrimitive(thing) {
    return Object(thing) !== thing;
}
function stringifyPrimitive(thing) {
    if (typeof thing === 'string')
        return stringifyString(thing);
    if (thing === void 0)
        return 'void 0';
    if (thing === 0 && 1 / thing < 0)
        return '-0';
    var str = String(thing);
    if (typeof thing === 'number')
        return str.replace(/^(-)?0\./, '$1.');
    return str;
}
function getType(thing) {
    return Object.prototype.toString.call(thing).slice(8, -1);
}
function escapeUnsafeChar(c) {
    return escaped$1[c] || c;
}
function escapeUnsafeChars(str) {
    return str.replace(unsafeChars, escapeUnsafeChar);
}
function safeKey(key) {
    return /^[_$a-zA-Z][_$a-zA-Z0-9]*$/.test(key) ? key : escapeUnsafeChars(JSON.stringify(key));
}
function safeProp(key) {
    return /^[_$a-zA-Z][_$a-zA-Z0-9]*$/.test(key) ? "." + key : "[" + escapeUnsafeChars(JSON.stringify(key)) + "]";
}
function stringifyString(str) {
    var result = '"';
    for (var i = 0; i < str.length; i += 1) {
        var char = str.charAt(i);
        var code = char.charCodeAt(0);
        if (char === '"') {
            result += '\\"';
        }
        else if (char in escaped$1) {
            result += escaped$1[char];
        }
        else if (code >= 0xd800 && code <= 0xdfff) {
            var next = str.charCodeAt(i + 1);
            // If this is the beginning of a [high, low] surrogate pair,
            // add the next two characters, otherwise escape
            if (code <= 0xdbff && (next >= 0xdc00 && next <= 0xdfff)) {
                result += char + str[++i];
            }
            else {
                result += "\\u" + code.toString(16).toUpperCase();
            }
        }
        else {
            result += char;
        }
    }
    result += '"';
    return result;
}

// Based on https://github.com/tmpvar/jsdom/blob/aa85b2abf07766ff7bf5c1f6daafb3726f2f2db5/lib/jsdom/living/blob.js

// fix for "Readable" isn't a named export issue
const Readable = Stream.Readable;

const BUFFER = Symbol('buffer');
const TYPE = Symbol('type');

class Blob {
	constructor() {
		this[TYPE] = '';

		const blobParts = arguments[0];
		const options = arguments[1];

		const buffers = [];
		let size = 0;

		if (blobParts) {
			const a = blobParts;
			const length = Number(a.length);
			for (let i = 0; i < length; i++) {
				const element = a[i];
				let buffer;
				if (element instanceof Buffer) {
					buffer = element;
				} else if (ArrayBuffer.isView(element)) {
					buffer = Buffer.from(element.buffer, element.byteOffset, element.byteLength);
				} else if (element instanceof ArrayBuffer) {
					buffer = Buffer.from(element);
				} else if (element instanceof Blob) {
					buffer = element[BUFFER];
				} else {
					buffer = Buffer.from(typeof element === 'string' ? element : String(element));
				}
				size += buffer.length;
				buffers.push(buffer);
			}
		}

		this[BUFFER] = Buffer.concat(buffers);

		let type = options && options.type !== undefined && String(options.type).toLowerCase();
		if (type && !/[^\u0020-\u007E]/.test(type)) {
			this[TYPE] = type;
		}
	}
	get size() {
		return this[BUFFER].length;
	}
	get type() {
		return this[TYPE];
	}
	text() {
		return Promise.resolve(this[BUFFER].toString());
	}
	arrayBuffer() {
		const buf = this[BUFFER];
		const ab = buf.buffer.slice(buf.byteOffset, buf.byteOffset + buf.byteLength);
		return Promise.resolve(ab);
	}
	stream() {
		const readable = new Readable();
		readable._read = function () {};
		readable.push(this[BUFFER]);
		readable.push(null);
		return readable;
	}
	toString() {
		return '[object Blob]';
	}
	slice() {
		const size = this.size;

		const start = arguments[0];
		const end = arguments[1];
		let relativeStart, relativeEnd;
		if (start === undefined) {
			relativeStart = 0;
		} else if (start < 0) {
			relativeStart = Math.max(size + start, 0);
		} else {
			relativeStart = Math.min(start, size);
		}
		if (end === undefined) {
			relativeEnd = size;
		} else if (end < 0) {
			relativeEnd = Math.max(size + end, 0);
		} else {
			relativeEnd = Math.min(end, size);
		}
		const span = Math.max(relativeEnd - relativeStart, 0);

		const buffer = this[BUFFER];
		const slicedBuffer = buffer.slice(relativeStart, relativeStart + span);
		const blob = new Blob([], { type: arguments[2] });
		blob[BUFFER] = slicedBuffer;
		return blob;
	}
}

Object.defineProperties(Blob.prototype, {
	size: { enumerable: true },
	type: { enumerable: true },
	slice: { enumerable: true }
});

Object.defineProperty(Blob.prototype, Symbol.toStringTag, {
	value: 'Blob',
	writable: false,
	enumerable: false,
	configurable: true
});

/**
 * fetch-error.js
 *
 * FetchError interface for operational errors
 */

/**
 * Create FetchError instance
 *
 * @param   String      message      Error message for human
 * @param   String      type         Error type for machine
 * @param   String      systemError  For Node.js system error
 * @return  FetchError
 */
function FetchError(message, type, systemError) {
  Error.call(this, message);

  this.message = message;
  this.type = type;

  // when err.type is `system`, err.code contains system error code
  if (systemError) {
    this.code = this.errno = systemError.code;
  }

  // hide custom error implementation details from end-users
  Error.captureStackTrace(this, this.constructor);
}

FetchError.prototype = Object.create(Error.prototype);
FetchError.prototype.constructor = FetchError;
FetchError.prototype.name = 'FetchError';

let convert;
try {
	convert = require('encoding').convert;
} catch (e) {}

const INTERNALS = Symbol('Body internals');

// fix an issue where "PassThrough" isn't a named export for node <10
const PassThrough = Stream.PassThrough;

/**
 * Body mixin
 *
 * Ref: https://fetch.spec.whatwg.org/#body
 *
 * @param   Stream  body  Readable stream
 * @param   Object  opts  Response options
 * @return  Void
 */
function Body(body) {
	var _this = this;

	var _ref = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
	    _ref$size = _ref.size;

	let size = _ref$size === undefined ? 0 : _ref$size;
	var _ref$timeout = _ref.timeout;
	let timeout = _ref$timeout === undefined ? 0 : _ref$timeout;

	if (body == null) {
		// body is undefined or null
		body = null;
	} else if (isURLSearchParams(body)) {
		// body is a URLSearchParams
		body = Buffer.from(body.toString());
	} else if (isBlob(body)) ; else if (Buffer.isBuffer(body)) ; else if (Object.prototype.toString.call(body) === '[object ArrayBuffer]') {
		// body is ArrayBuffer
		body = Buffer.from(body);
	} else if (ArrayBuffer.isView(body)) {
		// body is ArrayBufferView
		body = Buffer.from(body.buffer, body.byteOffset, body.byteLength);
	} else if (body instanceof Stream) ; else {
		// none of the above
		// coerce to string then buffer
		body = Buffer.from(String(body));
	}
	this[INTERNALS] = {
		body,
		disturbed: false,
		error: null
	};
	this.size = size;
	this.timeout = timeout;

	if (body instanceof Stream) {
		body.on('error', function (err) {
			const error = err.name === 'AbortError' ? err : new FetchError(`Invalid response body while trying to fetch ${_this.url}: ${err.message}`, 'system', err);
			_this[INTERNALS].error = error;
		});
	}
}

Body.prototype = {
	get body() {
		return this[INTERNALS].body;
	},

	get bodyUsed() {
		return this[INTERNALS].disturbed;
	},

	/**
  * Decode response as ArrayBuffer
  *
  * @return  Promise
  */
	arrayBuffer() {
		return consumeBody.call(this).then(function (buf) {
			return buf.buffer.slice(buf.byteOffset, buf.byteOffset + buf.byteLength);
		});
	},

	/**
  * Return raw response as Blob
  *
  * @return Promise
  */
	blob() {
		let ct = this.headers && this.headers.get('content-type') || '';
		return consumeBody.call(this).then(function (buf) {
			return Object.assign(
			// Prevent copying
			new Blob([], {
				type: ct.toLowerCase()
			}), {
				[BUFFER]: buf
			});
		});
	},

	/**
  * Decode response as json
  *
  * @return  Promise
  */
	json() {
		var _this2 = this;

		return consumeBody.call(this).then(function (buffer) {
			try {
				return JSON.parse(buffer.toString());
			} catch (err) {
				return Body.Promise.reject(new FetchError(`invalid json response body at ${_this2.url} reason: ${err.message}`, 'invalid-json'));
			}
		});
	},

	/**
  * Decode response as text
  *
  * @return  Promise
  */
	text() {
		return consumeBody.call(this).then(function (buffer) {
			return buffer.toString();
		});
	},

	/**
  * Decode response as buffer (non-spec api)
  *
  * @return  Promise
  */
	buffer() {
		return consumeBody.call(this);
	},

	/**
  * Decode response as text, while automatically detecting the encoding and
  * trying to decode to UTF-8 (non-spec api)
  *
  * @return  Promise
  */
	textConverted() {
		var _this3 = this;

		return consumeBody.call(this).then(function (buffer) {
			return convertBody(buffer, _this3.headers);
		});
	}
};

// In browsers, all properties are enumerable.
Object.defineProperties(Body.prototype, {
	body: { enumerable: true },
	bodyUsed: { enumerable: true },
	arrayBuffer: { enumerable: true },
	blob: { enumerable: true },
	json: { enumerable: true },
	text: { enumerable: true }
});

Body.mixIn = function (proto) {
	for (const name of Object.getOwnPropertyNames(Body.prototype)) {
		// istanbul ignore else: future proof
		if (!(name in proto)) {
			const desc = Object.getOwnPropertyDescriptor(Body.prototype, name);
			Object.defineProperty(proto, name, desc);
		}
	}
};

/**
 * Consume and convert an entire Body to a Buffer.
 *
 * Ref: https://fetch.spec.whatwg.org/#concept-body-consume-body
 *
 * @return  Promise
 */
function consumeBody() {
	var _this4 = this;

	if (this[INTERNALS].disturbed) {
		return Body.Promise.reject(new TypeError(`body used already for: ${this.url}`));
	}

	this[INTERNALS].disturbed = true;

	if (this[INTERNALS].error) {
		return Body.Promise.reject(this[INTERNALS].error);
	}

	let body = this.body;

	// body is null
	if (body === null) {
		return Body.Promise.resolve(Buffer.alloc(0));
	}

	// body is blob
	if (isBlob(body)) {
		body = body.stream();
	}

	// body is buffer
	if (Buffer.isBuffer(body)) {
		return Body.Promise.resolve(body);
	}

	// istanbul ignore if: should never happen
	if (!(body instanceof Stream)) {
		return Body.Promise.resolve(Buffer.alloc(0));
	}

	// body is stream
	// get ready to actually consume the body
	let accum = [];
	let accumBytes = 0;
	let abort = false;

	return new Body.Promise(function (resolve, reject) {
		let resTimeout;

		// allow timeout on slow response body
		if (_this4.timeout) {
			resTimeout = setTimeout(function () {
				abort = true;
				reject(new FetchError(`Response timeout while trying to fetch ${_this4.url} (over ${_this4.timeout}ms)`, 'body-timeout'));
			}, _this4.timeout);
		}

		// handle stream errors
		body.on('error', function (err) {
			if (err.name === 'AbortError') {
				// if the request was aborted, reject with this Error
				abort = true;
				reject(err);
			} else {
				// other errors, such as incorrect content-encoding
				reject(new FetchError(`Invalid response body while trying to fetch ${_this4.url}: ${err.message}`, 'system', err));
			}
		});

		body.on('data', function (chunk) {
			if (abort || chunk === null) {
				return;
			}

			if (_this4.size && accumBytes + chunk.length > _this4.size) {
				abort = true;
				reject(new FetchError(`content size at ${_this4.url} over limit: ${_this4.size}`, 'max-size'));
				return;
			}

			accumBytes += chunk.length;
			accum.push(chunk);
		});

		body.on('end', function () {
			if (abort) {
				return;
			}

			clearTimeout(resTimeout);

			try {
				resolve(Buffer.concat(accum, accumBytes));
			} catch (err) {
				// handle streams that have accumulated too much data (issue #414)
				reject(new FetchError(`Could not create Buffer from response body for ${_this4.url}: ${err.message}`, 'system', err));
			}
		});
	});
}

/**
 * Detect buffer encoding and convert to target encoding
 * ref: http://www.w3.org/TR/2011/WD-html5-20110113/parsing.html#determining-the-character-encoding
 *
 * @param   Buffer  buffer    Incoming buffer
 * @param   String  encoding  Target encoding
 * @return  String
 */
function convertBody(buffer, headers) {
	if (typeof convert !== 'function') {
		throw new Error('The package `encoding` must be installed to use the textConverted() function');
	}

	const ct = headers.get('content-type');
	let charset = 'utf-8';
	let res, str;

	// header
	if (ct) {
		res = /charset=([^;]*)/i.exec(ct);
	}

	// no charset in content type, peek at response body for at most 1024 bytes
	str = buffer.slice(0, 1024).toString();

	// html5
	if (!res && str) {
		res = /<meta.+?charset=(['"])(.+?)\1/i.exec(str);
	}

	// html4
	if (!res && str) {
		res = /<meta[\s]+?http-equiv=(['"])content-type\1[\s]+?content=(['"])(.+?)\2/i.exec(str);

		if (res) {
			res = /charset=(.*)/i.exec(res.pop());
		}
	}

	// xml
	if (!res && str) {
		res = /<\?xml.+?encoding=(['"])(.+?)\1/i.exec(str);
	}

	// found charset
	if (res) {
		charset = res.pop();

		// prevent decode issues when sites use incorrect encoding
		// ref: https://hsivonen.fi/encoding-menu/
		if (charset === 'gb2312' || charset === 'gbk') {
			charset = 'gb18030';
		}
	}

	// turn raw buffers into a single utf-8 buffer
	return convert(buffer, 'UTF-8', charset).toString();
}

/**
 * Detect a URLSearchParams object
 * ref: https://github.com/bitinn/node-fetch/issues/296#issuecomment-307598143
 *
 * @param   Object  obj     Object to detect by type or brand
 * @return  String
 */
function isURLSearchParams(obj) {
	// Duck-typing as a necessary condition.
	if (typeof obj !== 'object' || typeof obj.append !== 'function' || typeof obj.delete !== 'function' || typeof obj.get !== 'function' || typeof obj.getAll !== 'function' || typeof obj.has !== 'function' || typeof obj.set !== 'function') {
		return false;
	}

	// Brand-checking and more duck-typing as optional condition.
	return obj.constructor.name === 'URLSearchParams' || Object.prototype.toString.call(obj) === '[object URLSearchParams]' || typeof obj.sort === 'function';
}

/**
 * Check if `obj` is a W3C `Blob` object (which `File` inherits from)
 * @param  {*} obj
 * @return {boolean}
 */
function isBlob(obj) {
	return typeof obj === 'object' && typeof obj.arrayBuffer === 'function' && typeof obj.type === 'string' && typeof obj.stream === 'function' && typeof obj.constructor === 'function' && typeof obj.constructor.name === 'string' && /^(Blob|File)$/.test(obj.constructor.name) && /^(Blob|File)$/.test(obj[Symbol.toStringTag]);
}

/**
 * Clone body given Res/Req instance
 *
 * @param   Mixed  instance  Response or Request instance
 * @return  Mixed
 */
function clone(instance) {
	let p1, p2;
	let body = instance.body;

	// don't allow cloning a used body
	if (instance.bodyUsed) {
		throw new Error('cannot clone body after it is used');
	}

	// check that body is a stream and not form-data object
	// note: we can't clone the form-data object without having it as a dependency
	if (body instanceof Stream && typeof body.getBoundary !== 'function') {
		// tee instance body
		p1 = new PassThrough();
		p2 = new PassThrough();
		body.pipe(p1);
		body.pipe(p2);
		// set instance body to teed body and return the other teed body
		instance[INTERNALS].body = p1;
		body = p2;
	}

	return body;
}

/**
 * Performs the operation "extract a `Content-Type` value from |object|" as
 * specified in the specification:
 * https://fetch.spec.whatwg.org/#concept-bodyinit-extract
 *
 * This function assumes that instance.body is present.
 *
 * @param   Mixed  instance  Any options.body input
 */
function extractContentType(body) {
	if (body === null) {
		// body is null
		return null;
	} else if (typeof body === 'string') {
		// body is string
		return 'text/plain;charset=UTF-8';
	} else if (isURLSearchParams(body)) {
		// body is a URLSearchParams
		return 'application/x-www-form-urlencoded;charset=UTF-8';
	} else if (isBlob(body)) {
		// body is blob
		return body.type || null;
	} else if (Buffer.isBuffer(body)) {
		// body is buffer
		return null;
	} else if (Object.prototype.toString.call(body) === '[object ArrayBuffer]') {
		// body is ArrayBuffer
		return null;
	} else if (ArrayBuffer.isView(body)) {
		// body is ArrayBufferView
		return null;
	} else if (typeof body.getBoundary === 'function') {
		// detect form data input from form-data module
		return `multipart/form-data;boundary=${body.getBoundary()}`;
	} else if (body instanceof Stream) {
		// body is stream
		// can't really do much about this
		return null;
	} else {
		// Body constructor defaults other things to string
		return 'text/plain;charset=UTF-8';
	}
}

/**
 * The Fetch Standard treats this as if "total bytes" is a property on the body.
 * For us, we have to explicitly get it with a function.
 *
 * ref: https://fetch.spec.whatwg.org/#concept-body-total-bytes
 *
 * @param   Body    instance   Instance of Body
 * @return  Number?            Number of bytes, or null if not possible
 */
function getTotalBytes(instance) {
	const body = instance.body;


	if (body === null) {
		// body is null
		return 0;
	} else if (isBlob(body)) {
		return body.size;
	} else if (Buffer.isBuffer(body)) {
		// body is buffer
		return body.length;
	} else if (body && typeof body.getLengthSync === 'function') {
		// detect form data input from form-data module
		if (body._lengthRetrievers && body._lengthRetrievers.length == 0 || // 1.x
		body.hasKnownLength && body.hasKnownLength()) {
			// 2.x
			return body.getLengthSync();
		}
		return null;
	} else {
		// body is stream
		return null;
	}
}

/**
 * Write a Body to a Node.js WritableStream (e.g. http.Request) object.
 *
 * @param   Body    instance   Instance of Body
 * @return  Void
 */
function writeToStream(dest, instance) {
	const body = instance.body;


	if (body === null) {
		// body is null
		dest.end();
	} else if (isBlob(body)) {
		body.stream().pipe(dest);
	} else if (Buffer.isBuffer(body)) {
		// body is buffer
		dest.write(body);
		dest.end();
	} else {
		// body is stream
		body.pipe(dest);
	}
}

// expose Promise
Body.Promise = global.Promise;

/**
 * headers.js
 *
 * Headers class offers convenient helpers
 */

const invalidTokenRegex = /[^\^_`a-zA-Z\-0-9!#$%&'*+.|~]/;
const invalidHeaderCharRegex = /[^\t\x20-\x7e\x80-\xff]/;

function validateName(name) {
	name = `${name}`;
	if (invalidTokenRegex.test(name) || name === '') {
		throw new TypeError(`${name} is not a legal HTTP header name`);
	}
}

function validateValue(value) {
	value = `${value}`;
	if (invalidHeaderCharRegex.test(value)) {
		throw new TypeError(`${value} is not a legal HTTP header value`);
	}
}

/**
 * Find the key in the map object given a header name.
 *
 * Returns undefined if not found.
 *
 * @param   String  name  Header name
 * @return  String|Undefined
 */
function find(map, name) {
	name = name.toLowerCase();
	for (const key in map) {
		if (key.toLowerCase() === name) {
			return key;
		}
	}
	return undefined;
}

const MAP = Symbol('map');
class Headers {
	/**
  * Headers class
  *
  * @param   Object  headers  Response headers
  * @return  Void
  */
	constructor() {
		let init = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : undefined;

		this[MAP] = Object.create(null);

		if (init instanceof Headers) {
			const rawHeaders = init.raw();
			const headerNames = Object.keys(rawHeaders);

			for (const headerName of headerNames) {
				for (const value of rawHeaders[headerName]) {
					this.append(headerName, value);
				}
			}

			return;
		}

		// We don't worry about converting prop to ByteString here as append()
		// will handle it.
		if (init == null) ; else if (typeof init === 'object') {
			const method = init[Symbol.iterator];
			if (method != null) {
				if (typeof method !== 'function') {
					throw new TypeError('Header pairs must be iterable');
				}

				// sequence<sequence<ByteString>>
				// Note: per spec we have to first exhaust the lists then process them
				const pairs = [];
				for (const pair of init) {
					if (typeof pair !== 'object' || typeof pair[Symbol.iterator] !== 'function') {
						throw new TypeError('Each header pair must be iterable');
					}
					pairs.push(Array.from(pair));
				}

				for (const pair of pairs) {
					if (pair.length !== 2) {
						throw new TypeError('Each header pair must be a name/value tuple');
					}
					this.append(pair[0], pair[1]);
				}
			} else {
				// record<ByteString, ByteString>
				for (const key of Object.keys(init)) {
					const value = init[key];
					this.append(key, value);
				}
			}
		} else {
			throw new TypeError('Provided initializer must be an object');
		}
	}

	/**
  * Return combined header value given name
  *
  * @param   String  name  Header name
  * @return  Mixed
  */
	get(name) {
		name = `${name}`;
		validateName(name);
		const key = find(this[MAP], name);
		if (key === undefined) {
			return null;
		}

		return this[MAP][key].join(', ');
	}

	/**
  * Iterate over all headers
  *
  * @param   Function  callback  Executed for each item with parameters (value, name, thisArg)
  * @param   Boolean   thisArg   `this` context for callback function
  * @return  Void
  */
	forEach(callback) {
		let thisArg = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : undefined;

		let pairs = getHeaders(this);
		let i = 0;
		while (i < pairs.length) {
			var _pairs$i = pairs[i];
			const name = _pairs$i[0],
			      value = _pairs$i[1];

			callback.call(thisArg, value, name, this);
			pairs = getHeaders(this);
			i++;
		}
	}

	/**
  * Overwrite header values given name
  *
  * @param   String  name   Header name
  * @param   String  value  Header value
  * @return  Void
  */
	set(name, value) {
		name = `${name}`;
		value = `${value}`;
		validateName(name);
		validateValue(value);
		const key = find(this[MAP], name);
		this[MAP][key !== undefined ? key : name] = [value];
	}

	/**
  * Append a value onto existing header
  *
  * @param   String  name   Header name
  * @param   String  value  Header value
  * @return  Void
  */
	append(name, value) {
		name = `${name}`;
		value = `${value}`;
		validateName(name);
		validateValue(value);
		const key = find(this[MAP], name);
		if (key !== undefined) {
			this[MAP][key].push(value);
		} else {
			this[MAP][name] = [value];
		}
	}

	/**
  * Check for header name existence
  *
  * @param   String   name  Header name
  * @return  Boolean
  */
	has(name) {
		name = `${name}`;
		validateName(name);
		return find(this[MAP], name) !== undefined;
	}

	/**
  * Delete all header values given name
  *
  * @param   String  name  Header name
  * @return  Void
  */
	delete(name) {
		name = `${name}`;
		validateName(name);
		const key = find(this[MAP], name);
		if (key !== undefined) {
			delete this[MAP][key];
		}
	}

	/**
  * Return raw headers (non-spec api)
  *
  * @return  Object
  */
	raw() {
		return this[MAP];
	}

	/**
  * Get an iterator on keys.
  *
  * @return  Iterator
  */
	keys() {
		return createHeadersIterator(this, 'key');
	}

	/**
  * Get an iterator on values.
  *
  * @return  Iterator
  */
	values() {
		return createHeadersIterator(this, 'value');
	}

	/**
  * Get an iterator on entries.
  *
  * This is the default iterator of the Headers object.
  *
  * @return  Iterator
  */
	[Symbol.iterator]() {
		return createHeadersIterator(this, 'key+value');
	}
}
Headers.prototype.entries = Headers.prototype[Symbol.iterator];

Object.defineProperty(Headers.prototype, Symbol.toStringTag, {
	value: 'Headers',
	writable: false,
	enumerable: false,
	configurable: true
});

Object.defineProperties(Headers.prototype, {
	get: { enumerable: true },
	forEach: { enumerable: true },
	set: { enumerable: true },
	append: { enumerable: true },
	has: { enumerable: true },
	delete: { enumerable: true },
	keys: { enumerable: true },
	values: { enumerable: true },
	entries: { enumerable: true }
});

function getHeaders(headers) {
	let kind = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'key+value';

	const keys = Object.keys(headers[MAP]).sort();
	return keys.map(kind === 'key' ? function (k) {
		return k.toLowerCase();
	} : kind === 'value' ? function (k) {
		return headers[MAP][k].join(', ');
	} : function (k) {
		return [k.toLowerCase(), headers[MAP][k].join(', ')];
	});
}

const INTERNAL = Symbol('internal');

function createHeadersIterator(target, kind) {
	const iterator = Object.create(HeadersIteratorPrototype);
	iterator[INTERNAL] = {
		target,
		kind,
		index: 0
	};
	return iterator;
}

const HeadersIteratorPrototype = Object.setPrototypeOf({
	next() {
		// istanbul ignore if
		if (!this || Object.getPrototypeOf(this) !== HeadersIteratorPrototype) {
			throw new TypeError('Value of `this` is not a HeadersIterator');
		}

		var _INTERNAL = this[INTERNAL];
		const target = _INTERNAL.target,
		      kind = _INTERNAL.kind,
		      index = _INTERNAL.index;

		const values = getHeaders(target, kind);
		const len = values.length;
		if (index >= len) {
			return {
				value: undefined,
				done: true
			};
		}

		this[INTERNAL].index = index + 1;

		return {
			value: values[index],
			done: false
		};
	}
}, Object.getPrototypeOf(Object.getPrototypeOf([][Symbol.iterator]())));

Object.defineProperty(HeadersIteratorPrototype, Symbol.toStringTag, {
	value: 'HeadersIterator',
	writable: false,
	enumerable: false,
	configurable: true
});

/**
 * Export the Headers object in a form that Node.js can consume.
 *
 * @param   Headers  headers
 * @return  Object
 */
function exportNodeCompatibleHeaders(headers) {
	const obj = Object.assign({ __proto__: null }, headers[MAP]);

	// http.request() only supports string as Host header. This hack makes
	// specifying custom Host header possible.
	const hostHeaderKey = find(headers[MAP], 'Host');
	if (hostHeaderKey !== undefined) {
		obj[hostHeaderKey] = obj[hostHeaderKey][0];
	}

	return obj;
}

/**
 * Create a Headers object from an object of headers, ignoring those that do
 * not conform to HTTP grammar productions.
 *
 * @param   Object  obj  Object of headers
 * @return  Headers
 */
function createHeadersLenient(obj) {
	const headers = new Headers();
	for (const name of Object.keys(obj)) {
		if (invalidTokenRegex.test(name)) {
			continue;
		}
		if (Array.isArray(obj[name])) {
			for (const val of obj[name]) {
				if (invalidHeaderCharRegex.test(val)) {
					continue;
				}
				if (headers[MAP][name] === undefined) {
					headers[MAP][name] = [val];
				} else {
					headers[MAP][name].push(val);
				}
			}
		} else if (!invalidHeaderCharRegex.test(obj[name])) {
			headers[MAP][name] = [obj[name]];
		}
	}
	return headers;
}

const INTERNALS$1 = Symbol('Response internals');

// fix an issue where "STATUS_CODES" aren't a named export for node <10
const STATUS_CODES = http.STATUS_CODES;

/**
 * Response class
 *
 * @param   Stream  body  Readable stream
 * @param   Object  opts  Response options
 * @return  Void
 */
class Response {
	constructor() {
		let body = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
		let opts = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

		Body.call(this, body, opts);

		const status = opts.status || 200;
		const headers = new Headers(opts.headers);

		if (body != null && !headers.has('Content-Type')) {
			const contentType = extractContentType(body);
			if (contentType) {
				headers.append('Content-Type', contentType);
			}
		}

		this[INTERNALS$1] = {
			url: opts.url,
			status,
			statusText: opts.statusText || STATUS_CODES[status],
			headers,
			counter: opts.counter
		};
	}

	get url() {
		return this[INTERNALS$1].url || '';
	}

	get status() {
		return this[INTERNALS$1].status;
	}

	/**
  * Convenience property representing if the request ended normally
  */
	get ok() {
		return this[INTERNALS$1].status >= 200 && this[INTERNALS$1].status < 300;
	}

	get redirected() {
		return this[INTERNALS$1].counter > 0;
	}

	get statusText() {
		return this[INTERNALS$1].statusText;
	}

	get headers() {
		return this[INTERNALS$1].headers;
	}

	/**
  * Clone this response
  *
  * @return  Response
  */
	clone() {
		return new Response(clone(this), {
			url: this.url,
			status: this.status,
			statusText: this.statusText,
			headers: this.headers,
			ok: this.ok,
			redirected: this.redirected
		});
	}
}

Body.mixIn(Response.prototype);

Object.defineProperties(Response.prototype, {
	url: { enumerable: true },
	status: { enumerable: true },
	ok: { enumerable: true },
	redirected: { enumerable: true },
	statusText: { enumerable: true },
	headers: { enumerable: true },
	clone: { enumerable: true }
});

Object.defineProperty(Response.prototype, Symbol.toStringTag, {
	value: 'Response',
	writable: false,
	enumerable: false,
	configurable: true
});

const INTERNALS$2 = Symbol('Request internals');

// fix an issue where "format", "parse" aren't a named export for node <10
const parse_url = Url.parse;
const format_url = Url.format;

const streamDestructionSupported = 'destroy' in Stream.Readable.prototype;

/**
 * Check if a value is an instance of Request.
 *
 * @param   Mixed   input
 * @return  Boolean
 */
function isRequest(input) {
	return typeof input === 'object' && typeof input[INTERNALS$2] === 'object';
}

function isAbortSignal(signal) {
	const proto = signal && typeof signal === 'object' && Object.getPrototypeOf(signal);
	return !!(proto && proto.constructor.name === 'AbortSignal');
}

/**
 * Request class
 *
 * @param   Mixed   input  Url or Request instance
 * @param   Object  init   Custom options
 * @return  Void
 */
class Request {
	constructor(input) {
		let init = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

		let parsedURL;

		// normalize input
		if (!isRequest(input)) {
			if (input && input.href) {
				// in order to support Node.js' Url objects; though WHATWG's URL objects
				// will fall into this branch also (since their `toString()` will return
				// `href` property anyway)
				parsedURL = parse_url(input.href);
			} else {
				// coerce input to a string before attempting to parse
				parsedURL = parse_url(`${input}`);
			}
			input = {};
		} else {
			parsedURL = parse_url(input.url);
		}

		let method = init.method || input.method || 'GET';
		method = method.toUpperCase();

		if ((init.body != null || isRequest(input) && input.body !== null) && (method === 'GET' || method === 'HEAD')) {
			throw new TypeError('Request with GET/HEAD method cannot have body');
		}

		let inputBody = init.body != null ? init.body : isRequest(input) && input.body !== null ? clone(input) : null;

		Body.call(this, inputBody, {
			timeout: init.timeout || input.timeout || 0,
			size: init.size || input.size || 0
		});

		const headers = new Headers(init.headers || input.headers || {});

		if (inputBody != null && !headers.has('Content-Type')) {
			const contentType = extractContentType(inputBody);
			if (contentType) {
				headers.append('Content-Type', contentType);
			}
		}

		let signal = isRequest(input) ? input.signal : null;
		if ('signal' in init) signal = init.signal;

		if (signal != null && !isAbortSignal(signal)) {
			throw new TypeError('Expected signal to be an instanceof AbortSignal');
		}

		this[INTERNALS$2] = {
			method,
			redirect: init.redirect || input.redirect || 'follow',
			headers,
			parsedURL,
			signal
		};

		// node-fetch-only options
		this.follow = init.follow !== undefined ? init.follow : input.follow !== undefined ? input.follow : 20;
		this.compress = init.compress !== undefined ? init.compress : input.compress !== undefined ? input.compress : true;
		this.counter = init.counter || input.counter || 0;
		this.agent = init.agent || input.agent;
	}

	get method() {
		return this[INTERNALS$2].method;
	}

	get url() {
		return format_url(this[INTERNALS$2].parsedURL);
	}

	get headers() {
		return this[INTERNALS$2].headers;
	}

	get redirect() {
		return this[INTERNALS$2].redirect;
	}

	get signal() {
		return this[INTERNALS$2].signal;
	}

	/**
  * Clone this request
  *
  * @return  Request
  */
	clone() {
		return new Request(this);
	}
}

Body.mixIn(Request.prototype);

Object.defineProperty(Request.prototype, Symbol.toStringTag, {
	value: 'Request',
	writable: false,
	enumerable: false,
	configurable: true
});

Object.defineProperties(Request.prototype, {
	method: { enumerable: true },
	url: { enumerable: true },
	headers: { enumerable: true },
	redirect: { enumerable: true },
	clone: { enumerable: true },
	signal: { enumerable: true }
});

/**
 * Convert a Request to Node.js http request options.
 *
 * @param   Request  A Request instance
 * @return  Object   The options object to be passed to http.request
 */
function getNodeRequestOptions(request) {
	const parsedURL = request[INTERNALS$2].parsedURL;
	const headers = new Headers(request[INTERNALS$2].headers);

	// fetch step 1.3
	if (!headers.has('Accept')) {
		headers.set('Accept', '*/*');
	}

	// Basic fetch
	if (!parsedURL.protocol || !parsedURL.hostname) {
		throw new TypeError('Only absolute URLs are supported');
	}

	if (!/^https?:$/.test(parsedURL.protocol)) {
		throw new TypeError('Only HTTP(S) protocols are supported');
	}

	if (request.signal && request.body instanceof Stream.Readable && !streamDestructionSupported) {
		throw new Error('Cancellation of streamed requests with AbortSignal is not supported in node < 8');
	}

	// HTTP-network-or-cache fetch steps 2.4-2.7
	let contentLengthValue = null;
	if (request.body == null && /^(POST|PUT)$/i.test(request.method)) {
		contentLengthValue = '0';
	}
	if (request.body != null) {
		const totalBytes = getTotalBytes(request);
		if (typeof totalBytes === 'number') {
			contentLengthValue = String(totalBytes);
		}
	}
	if (contentLengthValue) {
		headers.set('Content-Length', contentLengthValue);
	}

	// HTTP-network-or-cache fetch step 2.11
	if (!headers.has('User-Agent')) {
		headers.set('User-Agent', 'node-fetch/1.0 (+https://github.com/bitinn/node-fetch)');
	}

	// HTTP-network-or-cache fetch step 2.15
	if (request.compress && !headers.has('Accept-Encoding')) {
		headers.set('Accept-Encoding', 'gzip,deflate');
	}

	let agent = request.agent;
	if (typeof agent === 'function') {
		agent = agent(parsedURL);
	}

	if (!headers.has('Connection') && !agent) {
		headers.set('Connection', 'close');
	}

	// HTTP-network fetch step 4.2
	// chunked encoding is handled by Node.js

	return Object.assign({}, parsedURL, {
		method: request.method,
		headers: exportNodeCompatibleHeaders(headers),
		agent
	});
}

/**
 * abort-error.js
 *
 * AbortError interface for cancelled requests
 */

/**
 * Create AbortError instance
 *
 * @param   String      message      Error message for human
 * @return  AbortError
 */
function AbortError(message) {
  Error.call(this, message);

  this.type = 'aborted';
  this.message = message;

  // hide custom error implementation details from end-users
  Error.captureStackTrace(this, this.constructor);
}

AbortError.prototype = Object.create(Error.prototype);
AbortError.prototype.constructor = AbortError;
AbortError.prototype.name = 'AbortError';

// fix an issue where "PassThrough", "resolve" aren't a named export for node <10
const PassThrough$1 = Stream.PassThrough;
const resolve_url = Url.resolve;

/**
 * Fetch function
 *
 * @param   Mixed    url   Absolute url or Request instance
 * @param   Object   opts  Fetch options
 * @return  Promise
 */
function fetch$1(url, opts) {

	// allow custom promise
	if (!fetch$1.Promise) {
		throw new Error('native promise missing, set fetch.Promise to your favorite alternative');
	}

	Body.Promise = fetch$1.Promise;

	// wrap http.request into fetch
	return new fetch$1.Promise(function (resolve, reject) {
		// build request object
		const request = new Request(url, opts);
		const options = getNodeRequestOptions(request);

		const send = (options.protocol === 'https:' ? https : http).request;
		const signal = request.signal;

		let response = null;

		const abort = function abort() {
			let error = new AbortError('The user aborted a request.');
			reject(error);
			if (request.body && request.body instanceof Stream.Readable) {
				request.body.destroy(error);
			}
			if (!response || !response.body) return;
			response.body.emit('error', error);
		};

		if (signal && signal.aborted) {
			abort();
			return;
		}

		const abortAndFinalize = function abortAndFinalize() {
			abort();
			finalize();
		};

		// send request
		const req = send(options);
		let reqTimeout;

		if (signal) {
			signal.addEventListener('abort', abortAndFinalize);
		}

		function finalize() {
			req.abort();
			if (signal) signal.removeEventListener('abort', abortAndFinalize);
			clearTimeout(reqTimeout);
		}

		if (request.timeout) {
			req.once('socket', function (socket) {
				reqTimeout = setTimeout(function () {
					reject(new FetchError(`network timeout at: ${request.url}`, 'request-timeout'));
					finalize();
				}, request.timeout);
			});
		}

		req.on('error', function (err) {
			reject(new FetchError(`request to ${request.url} failed, reason: ${err.message}`, 'system', err));
			finalize();
		});

		req.on('response', function (res) {
			clearTimeout(reqTimeout);

			const headers = createHeadersLenient(res.headers);

			// HTTP fetch step 5
			if (fetch$1.isRedirect(res.statusCode)) {
				// HTTP fetch step 5.2
				const location = headers.get('Location');

				// HTTP fetch step 5.3
				const locationURL = location === null ? null : resolve_url(request.url, location);

				// HTTP fetch step 5.5
				switch (request.redirect) {
					case 'error':
						reject(new FetchError(`redirect mode is set to error: ${request.url}`, 'no-redirect'));
						finalize();
						return;
					case 'manual':
						// node-fetch-specific step: make manual redirect a bit easier to use by setting the Location header value to the resolved URL.
						if (locationURL !== null) {
							// handle corrupted header
							try {
								headers.set('Location', locationURL);
							} catch (err) {
								// istanbul ignore next: nodejs server prevent invalid response headers, we can't test this through normal request
								reject(err);
							}
						}
						break;
					case 'follow':
						// HTTP-redirect fetch step 2
						if (locationURL === null) {
							break;
						}

						// HTTP-redirect fetch step 5
						if (request.counter >= request.follow) {
							reject(new FetchError(`maximum redirect reached at: ${request.url}`, 'max-redirect'));
							finalize();
							return;
						}

						// HTTP-redirect fetch step 6 (counter increment)
						// Create a new Request object.
						const requestOpts = {
							headers: new Headers(request.headers),
							follow: request.follow,
							counter: request.counter + 1,
							agent: request.agent,
							compress: request.compress,
							method: request.method,
							body: request.body,
							signal: request.signal,
							timeout: request.timeout
						};

						// HTTP-redirect fetch step 9
						if (res.statusCode !== 303 && request.body && getTotalBytes(request) === null) {
							reject(new FetchError('Cannot follow redirect with body being a readable stream', 'unsupported-redirect'));
							finalize();
							return;
						}

						// HTTP-redirect fetch step 11
						if (res.statusCode === 303 || (res.statusCode === 301 || res.statusCode === 302) && request.method === 'POST') {
							requestOpts.method = 'GET';
							requestOpts.body = undefined;
							requestOpts.headers.delete('content-length');
						}

						// HTTP-redirect fetch step 15
						resolve(fetch$1(new Request(locationURL, requestOpts)));
						finalize();
						return;
				}
			}

			// prepare response
			res.once('end', function () {
				if (signal) signal.removeEventListener('abort', abortAndFinalize);
			});
			let body = res.pipe(new PassThrough$1());

			const response_options = {
				url: request.url,
				status: res.statusCode,
				statusText: res.statusMessage,
				headers: headers,
				size: request.size,
				timeout: request.timeout,
				counter: request.counter
			};

			// HTTP-network fetch step 12.1.1.3
			const codings = headers.get('Content-Encoding');

			// HTTP-network fetch step 12.1.1.4: handle content codings

			// in following scenarios we ignore compression support
			// 1. compression support is disabled
			// 2. HEAD request
			// 3. no Content-Encoding header
			// 4. no content response (204)
			// 5. content not modified response (304)
			if (!request.compress || request.method === 'HEAD' || codings === null || res.statusCode === 204 || res.statusCode === 304) {
				response = new Response(body, response_options);
				resolve(response);
				return;
			}

			// For Node v6+
			// Be less strict when decoding compressed responses, since sometimes
			// servers send slightly invalid responses that are still accepted
			// by common browsers.
			// Always using Z_SYNC_FLUSH is what cURL does.
			const zlibOptions = {
				flush: zlib.Z_SYNC_FLUSH,
				finishFlush: zlib.Z_SYNC_FLUSH
			};

			// for gzip
			if (codings == 'gzip' || codings == 'x-gzip') {
				body = body.pipe(zlib.createGunzip(zlibOptions));
				response = new Response(body, response_options);
				resolve(response);
				return;
			}

			// for deflate
			if (codings == 'deflate' || codings == 'x-deflate') {
				// handle the infamous raw deflate response from old servers
				// a hack for old IIS and Apache servers
				const raw = res.pipe(new PassThrough$1());
				raw.once('data', function (chunk) {
					// see http://stackoverflow.com/questions/37519828
					if ((chunk[0] & 0x0F) === 0x08) {
						body = body.pipe(zlib.createInflate());
					} else {
						body = body.pipe(zlib.createInflateRaw());
					}
					response = new Response(body, response_options);
					resolve(response);
				});
				return;
			}

			// for br
			if (codings == 'br' && typeof zlib.createBrotliDecompress === 'function') {
				body = body.pipe(zlib.createBrotliDecompress());
				response = new Response(body, response_options);
				resolve(response);
				return;
			}

			// otherwise, use response as-is
			response = new Response(body, response_options);
			resolve(response);
		});

		writeToStream(req, request);
	});
}
/**
 * Redirect code matching
 *
 * @param   Number   code  Status code
 * @return  Boolean
 */
fetch$1.isRedirect = function (code) {
	return code === 301 || code === 302 || code === 303 || code === 307 || code === 308;
};

// expose Promise
fetch$1.Promise = global.Promise;

function get_page_handler(
	manifest,
	session_getter
) {
	const get_build_info =  (assets => () => assets)(JSON.parse(fs.readFileSync(path.join(build_dir, 'build.json'), 'utf-8')));

	const template =  (str => () => str)(read_template(build_dir));

	const has_service_worker = fs.existsSync(path.join(build_dir, 'service-worker.js'));

	const { server_routes, pages } = manifest;
	const error_route = manifest.error;

	function bail(req, res, err) {
		console.error(err);

		const message =  'Internal server error';

		res.statusCode = 500;
		res.end(`<pre>${message}</pre>`);
	}

	function handle_error(req, res, statusCode, error) {
		handle_page({
			pattern: null,
			parts: [
				{ name: null, component: error_route }
			]
		}, req, res, statusCode, error || new Error('Unknown error in preload function'));
	}

	async function handle_page(page, req, res, status = 200, error = null) {
		const is_service_worker_index = req.path === '/service-worker-index.html';
		const build_info




 = get_build_info();

		res.setHeader('Content-Type', 'text/html');
		res.setHeader('Cache-Control',  'max-age=600');

		// preload main.js and current route
		// TODO detect other stuff we can preload? images, CSS, fonts?
		let preloaded_chunks = Array.isArray(build_info.assets.main) ? build_info.assets.main : [build_info.assets.main];
		if (!error && !is_service_worker_index) {
			page.parts.forEach(part => {
				if (!part) return;

				// using concat because it could be a string or an array. thanks webpack!
				preloaded_chunks = preloaded_chunks.concat(build_info.assets[part.name]);
			});
		}

		if (build_info.bundler === 'rollup') {
			// TODO add dependencies and CSS
			const link = preloaded_chunks
				.filter(file => file && !file.match(/\.map$/))
				.map(file => `<${req.baseUrl}/client/${file}>;rel="modulepreload"`)
				.join(', ');

			res.setHeader('Link', link);
		} else {
			const link = preloaded_chunks
				.filter(file => file && !file.match(/\.map$/))
				.map((file) => {
					const as = /\.css$/.test(file) ? 'style' : 'script';
					return `<${req.baseUrl}/client/${file}>;rel="preload";as="${as}"`;
				})
				.join(', ');

			res.setHeader('Link', link);
		}

		let session;
		try {
			session = await session_getter(req, res);
		} catch (err) {
			return bail(req, res, err);
		}

		let redirect;
		let preload_error;

		const preload_context = {
			redirect: (statusCode, location) => {
				if (redirect && (redirect.statusCode !== statusCode || redirect.location !== location)) {
					throw new Error(`Conflicting redirects`);
				}
				location = location.replace(/^\//g, ''); // leading slash (only)
				redirect = { statusCode, location };
			},
			error: (statusCode, message) => {
				preload_error = { statusCode, message };
			},
			fetch: (url, opts) => {
				const parsed = new Url.URL(url, `http://127.0.0.1:${process.env.PORT}${req.baseUrl ? req.baseUrl + '/' :''}`);

				opts = Object.assign({}, opts);

				const include_credentials = (
					opts.credentials === 'include' ||
					opts.credentials !== 'omit' && parsed.origin === `http://127.0.0.1:${process.env.PORT}`
				);

				if (include_credentials) {
					opts.headers = Object.assign({}, opts.headers);

					const cookies = Object.assign(
						{},
						cookie.parse(req.headers.cookie || ''),
						cookie.parse(opts.headers.cookie || '')
					);

					const set_cookie = res.getHeader('Set-Cookie');
					(Array.isArray(set_cookie) ? set_cookie : [set_cookie]).forEach(str => {
						const match = /([^=]+)=([^;]+)/.exec(str);
						if (match) cookies[match[1]] = match[2];
					});

					const str = Object.keys(cookies)
						.map(key => `${key}=${cookies[key]}`)
						.join('; ');

					opts.headers.cookie = str;

					if (!opts.headers.authorization && req.headers.authorization) {
						opts.headers.authorization = req.headers.authorization;
					}
				}

				return fetch$1(parsed.href, opts);
			}
		};

		let preloaded;
		let match;
		let params;

		try {
			const root_preloaded = manifest.root_preload
				? manifest.root_preload.call(preload_context, {
					host: req.headers.host,
					path: req.path,
					query: req.query,
					params: {}
				}, session)
				: {};

			match = error ? null : page.pattern.exec(req.path);


			let toPreload = [root_preloaded];
			if (!is_service_worker_index) {
				toPreload = toPreload.concat(page.parts.map(part => {
					if (!part) return null;

					// the deepest level is used below, to initialise the store
					params = part.params ? part.params(match) : {};

					return part.preload
						? part.preload.call(preload_context, {
							host: req.headers.host,
							path: req.path,
							query: req.query,
							params
						}, session)
						: {};
				}));
			}

			preloaded = await Promise.all(toPreload);
		} catch (err) {
			if (error) {
				return bail(req, res, err)
			}

			preload_error = { statusCode: 500, message: err };
			preloaded = []; // appease TypeScript
		}

		try {
			if (redirect) {
				const location = Url.resolve((req.baseUrl || '') + '/', redirect.location);

				res.statusCode = redirect.statusCode;
				res.setHeader('Location', location);
				res.end();

				return;
			}

			if (preload_error) {
				handle_error(req, res, preload_error.statusCode, preload_error.message);
				return;
			}

			const segments = req.path.split('/').filter(Boolean);

			// TODO make this less confusing
			const layout_segments = [segments[0]];
			let l = 1;

			page.parts.forEach((part, i) => {
				layout_segments[l] = segments[i + 1];
				if (!part) return null;
				l++;
			});

			const props = {
				stores: {
					page: {
						subscribe: writable({
							host: req.headers.host,
							path: req.path,
							query: req.query,
							params
						}).subscribe
					},
					preloading: {
						subscribe: writable(null).subscribe
					},
					session: writable(session)
				},
				segments: layout_segments,
				status: error ? status : 200,
				error: error ? error instanceof Error ? error : { message: error } : null,
				level0: {
					props: preloaded[0]
				},
				level1: {
					segment: segments[0],
					props: {}
				}
			};

			if (!is_service_worker_index) {
				let l = 1;
				for (let i = 0; i < page.parts.length; i += 1) {
					const part = page.parts[i];
					if (!part) continue;

					props[`level${l++}`] = {
						component: part.component,
						props: preloaded[i + 1] || {},
						segment: segments[i]
					};
				}
			}

			const { html, head, css } = App.render(props);

			const serialized = {
				preloaded: `[${preloaded.map(data => try_serialize(data)).join(',')}]`,
				session: session && try_serialize(session, err => {
					throw new Error(`Failed to serialize session data: ${err.message}`);
				}),
				error: error && serialize_error(props.error)
			};

			let script = `__SAPPER__={${[
				error && `error:${serialized.error},status:${status}`,
				`baseUrl:"${req.baseUrl}"`,
				serialized.preloaded && `preloaded:${serialized.preloaded}`,
				serialized.session && `session:${serialized.session}`
			].filter(Boolean).join(',')}};`;

			if (has_service_worker) {
				script += `if('serviceWorker' in navigator)navigator.serviceWorker.register('${req.baseUrl}/service-worker.js');`;
			}

			const file = [].concat(build_info.assets.main).filter(file => file && /\.js$/.test(file))[0];
			const main = `${req.baseUrl}/client/${file}`;

			if (build_info.bundler === 'rollup') {
				if (build_info.legacy_assets) {
					const legacy_main = `${req.baseUrl}/client/legacy/${build_info.legacy_assets.main}`;
					script += `(function(){try{eval("async function x(){}");var main="${main}"}catch(e){main="${legacy_main}"};var s=document.createElement("script");try{new Function("if(0)import('')")();s.src=main;s.type="module";s.crossOrigin="use-credentials";}catch(e){s.src="${req.baseUrl}/client/shimport@${build_info.shimport}.js";s.setAttribute("data-main",main);}document.head.appendChild(s);}());`;
				} else {
					script += `var s=document.createElement("script");try{new Function("if(0)import('')")();s.src="${main}";s.type="module";s.crossOrigin="use-credentials";}catch(e){s.src="${req.baseUrl}/client/shimport@${build_info.shimport}.js";s.setAttribute("data-main","${main}")}document.head.appendChild(s)`;
				}
			} else {
				script += `</script><script src="${main}">`;
			}

			let styles;

			// TODO make this consistent across apps
			// TODO embed build_info in placeholder.ts
			if (build_info.css && build_info.css.main) {
				const css_chunks = new Set();
				if (build_info.css.main) css_chunks.add(build_info.css.main);
				page.parts.forEach(part => {
					if (!part) return;
					const css_chunks_for_part = build_info.css.chunks[part.file];

					if (css_chunks_for_part) {
						css_chunks_for_part.forEach(file => {
							css_chunks.add(file);
						});
					}
				});

				styles = Array.from(css_chunks)
					.map(href => `<link rel="stylesheet" href="client/${href}">`)
					.join('');
			} else {
				styles = (css && css.code ? `<style>${css.code}</style>` : '');
			}

			// users can set a CSP nonce using res.locals.nonce
			const nonce_attr = (res.locals && res.locals.nonce) ? ` nonce="${res.locals.nonce}"` : '';

			const body = template()
				.replace('%sapper.base%', () => `<base href="${req.baseUrl}/">`)
				.replace('%sapper.scripts%', () => `<script${nonce_attr}>${script}</script>`)
				.replace('%sapper.html%', () => html)
				.replace('%sapper.head%', () => `<noscript id='sapper-head-start'></noscript>${head}<noscript id='sapper-head-end'></noscript>`)
				.replace('%sapper.styles%', () => styles);

			res.statusCode = status;
			res.end(body);
		} catch(err) {
			if (error) {
				bail(req, res, err);
			} else {
				handle_error(req, res, 500, err);
			}
		}
	}

	return function find_route(req, res, next) {
		if (req.path === '/service-worker-index.html') {
			const homePage = pages.find(page => page.pattern.test('/'));
			handle_page(homePage, req, res);
			return;
		}

		for (const page of pages) {
			if (page.pattern.test(req.path)) {
				handle_page(page, req, res);
				return;
			}
		}

		handle_error(req, res, 404, 'Not found');
	};
}

function read_template(dir = build_dir) {
	return fs.readFileSync(`${dir}/template.html`, 'utf-8');
}

function try_serialize(data, fail) {
	try {
		return devalue(data);
	} catch (err) {
		if (fail) fail(err);
		return null;
	}
}

// Ensure we return something truthy so the client will not re-render the page over the error
function serialize_error(error) {
	if (!error) return null;
	let serialized = try_serialize(error);
	if (!serialized) {
		const { name, message, stack } = error ;
		serialized = try_serialize({ name, message, stack });
	}
	if (!serialized) {
		serialized = '{}';
	}
	return serialized;
}

function middleware(opts


 = {}) {
	const { session, ignore } = opts;

	let emitted_basepath = false;

	return compose_handlers(ignore, [
		(req, res, next) => {
			if (req.baseUrl === undefined) {
				let { originalUrl } = req;
				if (req.url === '/' && originalUrl[originalUrl.length - 1] !== '/') {
					originalUrl += '/';
				}

				req.baseUrl = originalUrl
					? originalUrl.slice(0, -req.url.length)
					: '';
			}

			if (!emitted_basepath && process.send) {
				process.send({
					__sapper__: true,
					event: 'basepath',
					basepath: req.baseUrl
				});

				emitted_basepath = true;
			}

			if (req.path === undefined) {
				req.path = req.url.replace(/\?.*/, '');
			}

			next();
		},

		fs.existsSync(path.join(build_dir, 'service-worker.js')) && serve({
			pathname: '/service-worker.js',
			cache_control: 'no-cache, no-store, must-revalidate'
		}),

		fs.existsSync(path.join(build_dir, 'service-worker.js.map')) && serve({
			pathname: '/service-worker.js.map',
			cache_control: 'no-cache, no-store, must-revalidate'
		}),

		serve({
			prefix: '/client/',
			cache_control:  'max-age=31536000, immutable'
		}),

		get_server_route_handler(manifest.server_routes),

		get_page_handler(manifest, session || noop$1)
	].filter(Boolean));
}

function compose_handlers(ignore, handlers) {
	const total = handlers.length;

	function nth_handler(n, req, res, next) {
		if (n >= total) {
			return next();
		}

		handlers[n](req, res, () => nth_handler(n+1, req, res, next));
	}

	return !ignore
		? (req, res, next) => nth_handler(0, req, res, next)
		: (req, res, next) => {
			if (should_ignore(req.path, ignore)) {
				next();
			} else {
				nth_handler(0, req, res, next);
			}
		};
}

function should_ignore(uri, val) {
	if (Array.isArray(val)) return val.some(x => should_ignore(uri, x));
	if (val instanceof RegExp) return val.test(uri);
	if (typeof val === 'function') return val(uri);
	return uri.startsWith(val.charCodeAt(0) === 47 ? val : `/${val}`);
}

function serve({ prefix, pathname, cache_control }



) {
	const filter = pathname
		? (req) => req.path === pathname
		: (req) => req.path.startsWith(prefix);

	const cache = new Map();

	const read =  (file) => (cache.has(file) ? cache : cache.set(file, fs.readFileSync(path.join(build_dir, file)))).get(file);

	return (req, res, next) => {
		if (filter(req)) {
			const type = lite.getType(req.path);

			try {
				const file = path.posix.normalize(decodeURIComponent(req.path));
				const data = read(file);

				res.setHeader('Content-Type', type);
				res.setHeader('Cache-Control', cache_control);
				res.end(data);
			} catch (err) {
				res.statusCode = 404;
				res.end('not found');
			}
		} else {
			next();
		}
	};
}

function noop$1(){}

const { PORT, NODE_ENV } = process.env;
const dev = NODE_ENV === 'development';

polka() // You can also use Express
	.use(
		compression({ threshold: 0 }),
		sirv('static', { dev }),
		middleware()
	)
	.listen(PORT, err => {
		if (err) console.log('error', err);
	});
