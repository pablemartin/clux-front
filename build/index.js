// generated by sapper build at 2020-07-10T00:06:44.947Z
process.env.NODE_ENV = process.env.NODE_ENV || 'production';
process.env.PORT = process.env.PORT || 80;

console.log('Starting server on port ' + process.env.PORT);
require('./server/server.js');
