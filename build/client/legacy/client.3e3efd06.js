function t(n){return(t="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(t){return typeof t}:function(t){return t&&"function"==typeof Symbol&&t.constructor===Symbol&&t!==Symbol.prototype?"symbol":typeof t})(n)}function n(t,n){(null==n||n>t.length)&&(n=t.length);for(var e=0,r=new Array(n);e<n;e++)r[e]=t[e];return r}function e(t,e){if(t){if("string"==typeof t)return n(t,e);var r=Object.prototype.toString.call(t).slice(8,-1);return"Object"===r&&t.constructor&&(r=t.constructor.name),"Map"===r||"Set"===r?Array.from(t):"Arguments"===r||/^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(r)?n(t,e):void 0}}function r(t,n){return function(t){if(Array.isArray(t))return t}(t)||function(t,n){if("undefined"!=typeof Symbol&&Symbol.iterator in Object(t)){var e=[],r=!0,o=!1,a=void 0;try{for(var i,c=t[Symbol.iterator]();!(r=(i=c.next()).done)&&(e.push(i.value),!n||e.length!==n);r=!0);}catch(t){o=!0,a=t}finally{try{r||null==c.return||c.return()}finally{if(o)throw a}}return e}}(t,n)||e(t,n)||function(){throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")}()}var o,a=function(t,n){return t(n={exports:{}},n.exports),n.exports}((function(n){var e=function(n){var e=Object.prototype,r=e.hasOwnProperty,o="function"==typeof Symbol?Symbol:{},a=o.iterator||"@@iterator",i=o.asyncIterator||"@@asyncIterator",c=o.toStringTag||"@@toStringTag";function u(t,n,e,r){var o=n&&n.prototype instanceof l?n:l,a=Object.create(o.prototype),i=new $(r||[]);return a._invoke=function(t,n,e){var r="suspendedStart";return function(o,a){if("executing"===r)throw new Error("Generator is already running");if("completed"===r){if("throw"===o)throw a;return S()}for(e.method=o,e.arg=a;;){var i=e.delegate;if(i){var c=w(i,e);if(c){if(c===f)continue;return c}}if("next"===e.method)e.sent=e._sent=e.arg;else if("throw"===e.method){if("suspendedStart"===r)throw r="completed",e.arg;e.dispatchException(e.arg)}else"return"===e.method&&e.abrupt("return",e.arg);r="executing";var u=s(t,n,e);if("normal"===u.type){if(r=e.done?"completed":"suspendedYield",u.arg===f)continue;return{value:u.arg,done:e.done}}"throw"===u.type&&(r="completed",e.method="throw",e.arg=u.arg)}}}(t,e,i),a}function s(t,n,e){try{return{type:"normal",arg:t.call(n,e)}}catch(t){return{type:"throw",arg:t}}}n.wrap=u;var f={};function l(){}function p(){}function h(){}var d={};d[a]=function(){return this};var v=Object.getPrototypeOf,m=v&&v(v(_([])));m&&m!==e&&r.call(m,a)&&(d=m);var y=h.prototype=l.prototype=Object.create(d);function g(t){["next","throw","return"].forEach((function(n){t[n]=function(t){return this._invoke(n,t)}}))}function b(n,e){var o;this._invoke=function(a,i){function c(){return new e((function(o,c){!function o(a,i,c,u){var f=s(n[a],n,i);if("throw"!==f.type){var l=f.arg,p=l.value;return p&&"object"===t(p)&&r.call(p,"__await")?e.resolve(p.__await).then((function(t){o("next",t,c,u)}),(function(t){o("throw",t,c,u)})):e.resolve(p).then((function(t){l.value=t,c(l)}),(function(t){return o("throw",t,c,u)}))}u(f.arg)}(a,i,o,c)}))}return o=o?o.then(c,c):c()}}function w(t,n){var e=t.iterator[n.method];if(void 0===e){if(n.delegate=null,"throw"===n.method){if(t.iterator.return&&(n.method="return",n.arg=void 0,w(t,n),"throw"===n.method))return f;n.method="throw",n.arg=new TypeError("The iterator does not provide a 'throw' method")}return f}var r=s(e,t.iterator,n.arg);if("throw"===r.type)return n.method="throw",n.arg=r.arg,n.delegate=null,f;var o=r.arg;return o?o.done?(n[t.resultName]=o.value,n.next=t.nextLoc,"return"!==n.method&&(n.method="next",n.arg=void 0),n.delegate=null,f):o:(n.method="throw",n.arg=new TypeError("iterator result is not an object"),n.delegate=null,f)}function E(t){var n={tryLoc:t[0]};1 in t&&(n.catchLoc=t[1]),2 in t&&(n.finallyLoc=t[2],n.afterLoc=t[3]),this.tryEntries.push(n)}function x(t){var n=t.completion||{};n.type="normal",delete n.arg,t.completion=n}function $(t){this.tryEntries=[{tryLoc:"root"}],t.forEach(E,this),this.reset(!0)}function _(t){if(t){var n=t[a];if(n)return n.call(t);if("function"==typeof t.next)return t;if(!isNaN(t.length)){var e=-1,o=function n(){for(;++e<t.length;)if(r.call(t,e))return n.value=t[e],n.done=!1,n;return n.value=void 0,n.done=!0,n};return o.next=o}}return{next:S}}function S(){return{value:void 0,done:!0}}return p.prototype=y.constructor=h,h.constructor=p,h[c]=p.displayName="GeneratorFunction",n.isGeneratorFunction=function(t){var n="function"==typeof t&&t.constructor;return!!n&&(n===p||"GeneratorFunction"===(n.displayName||n.name))},n.mark=function(t){return Object.setPrototypeOf?Object.setPrototypeOf(t,h):(t.__proto__=h,c in t||(t[c]="GeneratorFunction")),t.prototype=Object.create(y),t},n.awrap=function(t){return{__await:t}},g(b.prototype),b.prototype[i]=function(){return this},n.AsyncIterator=b,n.async=function(t,e,r,o,a){void 0===a&&(a=Promise);var i=new b(u(t,e,r,o),a);return n.isGeneratorFunction(e)?i:i.next().then((function(t){return t.done?t.value:i.next()}))},g(y),y[c]="Generator",y[a]=function(){return this},y.toString=function(){return"[object Generator]"},n.keys=function(t){var n=[];for(var e in t)n.push(e);return n.reverse(),function e(){for(;n.length;){var r=n.pop();if(r in t)return e.value=r,e.done=!1,e}return e.done=!0,e}},n.values=_,$.prototype={constructor:$,reset:function(t){if(this.prev=0,this.next=0,this.sent=this._sent=void 0,this.done=!1,this.delegate=null,this.method="next",this.arg=void 0,this.tryEntries.forEach(x),!t)for(var n in this)"t"===n.charAt(0)&&r.call(this,n)&&!isNaN(+n.slice(1))&&(this[n]=void 0)},stop:function(){this.done=!0;var t=this.tryEntries[0].completion;if("throw"===t.type)throw t.arg;return this.rval},dispatchException:function(t){if(this.done)throw t;var n=this;function e(e,r){return i.type="throw",i.arg=t,n.next=e,r&&(n.method="next",n.arg=void 0),!!r}for(var o=this.tryEntries.length-1;o>=0;--o){var a=this.tryEntries[o],i=a.completion;if("root"===a.tryLoc)return e("end");if(a.tryLoc<=this.prev){var c=r.call(a,"catchLoc"),u=r.call(a,"finallyLoc");if(c&&u){if(this.prev<a.catchLoc)return e(a.catchLoc,!0);if(this.prev<a.finallyLoc)return e(a.finallyLoc)}else if(c){if(this.prev<a.catchLoc)return e(a.catchLoc,!0)}else{if(!u)throw new Error("try statement without catch or finally");if(this.prev<a.finallyLoc)return e(a.finallyLoc)}}}},abrupt:function(t,n){for(var e=this.tryEntries.length-1;e>=0;--e){var o=this.tryEntries[e];if(o.tryLoc<=this.prev&&r.call(o,"finallyLoc")&&this.prev<o.finallyLoc){var a=o;break}}a&&("break"===t||"continue"===t)&&a.tryLoc<=n&&n<=a.finallyLoc&&(a=null);var i=a?a.completion:{};return i.type=t,i.arg=n,a?(this.method="next",this.next=a.finallyLoc,f):this.complete(i)},complete:function(t,n){if("throw"===t.type)throw t.arg;return"break"===t.type||"continue"===t.type?this.next=t.arg:"return"===t.type?(this.rval=this.arg=t.arg,this.method="return",this.next="end"):"normal"===t.type&&n&&(this.next=n),f},finish:function(t){for(var n=this.tryEntries.length-1;n>=0;--n){var e=this.tryEntries[n];if(e.finallyLoc===t)return this.complete(e.completion,e.afterLoc),x(e),f}},catch:function(t){for(var n=this.tryEntries.length-1;n>=0;--n){var e=this.tryEntries[n];if(e.tryLoc===t){var r=e.completion;if("throw"===r.type){var o=r.arg;x(e)}return o}}throw new Error("illegal catch attempt")},delegateYield:function(t,n,e){return this.delegate={iterator:_(t),resultName:n,nextLoc:e},"next"===this.method&&(this.arg=void 0),f}},n}(n.exports);try{regeneratorRuntime=e}catch(t){Function("r","regeneratorRuntime = r")(e)}}));function i(t,n,e,r,o,a,i){try{var c=t[a](i),u=c.value}catch(t){return void e(t)}c.done?n(u):Promise.resolve(u).then(r,o)}function c(t){return function(){var n=this,e=arguments;return new Promise((function(r,o){var a=t.apply(n,e);function c(t){i(a,r,o,c,u,"next",t)}function u(t){i(a,r,o,c,u,"throw",t)}c(void 0)}))}}function u(t){return(u=Object.setPrototypeOf?Object.getPrototypeOf:function(t){return t.__proto__||Object.getPrototypeOf(t)})(t)}function s(t,n){return(s=Object.setPrototypeOf||function(t,n){return t.__proto__=n,t})(t,n)}function f(t,n){if("function"!=typeof n&&null!==n)throw new TypeError("Super expression must either be null or a function");t.prototype=Object.create(n&&n.prototype,{constructor:{value:t,writable:!0,configurable:!0}}),n&&s(t,n)}function l(t){if(void 0===t)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return t}function p(n,e){return!e||"object"!==t(e)&&"function"!=typeof e?l(n):e}function h(t){return function(t){if(Array.isArray(t))return n(t)}(t)||function(t){if("undefined"!=typeof Symbol&&Symbol.iterator in Object(t))return Array.from(t)}(t)||e(t)||function(){throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")}()}function d(t,n){if(!(t instanceof n))throw new TypeError("Cannot call a class as a function")}function v(t,n){for(var e=0;e<n.length;e++){var r=n[e];r.enumerable=r.enumerable||!1,r.configurable=!0,"value"in r&&(r.writable=!0),Object.defineProperty(t,r.key,r)}}function m(){}function y(t,n){for(var e in n)t[e]=n[e];return t}function g(t){return t()}function b(){return Object.create(null)}function w(t){t.forEach(g)}function E(t){return"function"==typeof t}function x(n,e){return n!=n?e==e:n!==e||n&&"object"===t(n)||"function"==typeof n}function $(t,n,e){t.$$.on_destroy.push(function(t){if(null==t)return m;for(var n=arguments.length,e=new Array(n>1?n-1:0),r=1;r<n;r++)e[r-1]=arguments[r];var o=t.subscribe.apply(t,e);return o.unsubscribe?function(){return o.unsubscribe()}:o}(n,e))}function _(t,n,e,r){if(t){var o=S(t,n,e,r);return t[0](o)}}function S(t,n,e,r){return t[1]&&r?y(e.ctx.slice(),t[1](r(n))):e.ctx}function L(n,e,r,o,a,i,c){var u=function(n,e,r,o){if(n[2]&&o){var a=n[2](o(r));if(void 0===e.dirty)return a;if("object"===t(a)){for(var i=[],c=Math.max(e.dirty.length,a.length),u=0;u<c;u+=1)i[u]=e.dirty[u]|a[u];return i}return e.dirty|a}return e.dirty}(e,o,a,i);if(u){var s=S(e,r,o,c);n.p(s,u)}}function A(t){return t&&E(t.destroy)?t.destroy:m}function R(t,n){t.appendChild(n)}function P(t,n,e){t.insertBefore(n,e||null)}function k(t){t.parentNode.removeChild(t)}function j(t){return document.createElement(t)}function O(t){return document.createTextNode(t)}function C(){return O(" ")}function I(){return O("")}function D(t,n,e,r){return t.addEventListener(n,e,r),function(){return t.removeEventListener(n,e,r)}}function N(t,n,e){null==e?t.removeAttribute(n):t.getAttribute(n)!==e&&t.setAttribute(n,e)}function q(t){return Array.from(t.childNodes)}function U(t,n,e,r){for(var o=0;o<t.length;o+=1){var a=t[o];if(a.nodeName===n){for(var i=0,c=[];i<a.attributes.length;){var u=a.attributes[i++];e[u.name]||c.push(u.name)}for(var s=0;s<c.length;s++)a.removeAttribute(c[s]);return t.splice(o,1)[0]}}return r?function(t){return document.createElementNS("http://www.w3.org/2000/svg",t)}(n):j(n)}function T(t,n){for(var e=0;e<t.length;e+=1){var r=t[e];if(3===r.nodeType)return r.data=""+n,t.splice(e,1)[0]}return O(n)}function G(t){return T(t," ")}function F(t,n){n=""+n,t.wholeText!==n&&(t.data=n)}function H(t,n){t.value=null==n?"":n}function V(t,n,e,r){t.style.setProperty(n,e,r?"important":"")}function B(t,n,e){t.classList[e?"add":"remove"](n)}function J(t){var n=arguments.length>1&&void 0!==arguments[1]?arguments[1]:document.body;return Array.from(n.querySelectorAll(t))}function M(t){o=t}function K(){if(!o)throw new Error("Function called outside component initialization");return o}function Y(){var t=K();return function(n,e){var r=t.$$.callbacks[n];if(r){var o=function(t,n){var e=document.createEvent("CustomEvent");return e.initCustomEvent(t,!1,!1,n),e}(n,e);r.slice().forEach((function(n){n.call(t,o)}))}}}var z=[],Q=[],W=[],X=[],Z=Promise.resolve(),tt=!1;function nt(t){W.push(t)}var et=!1,rt=new Set;function ot(){if(!et){et=!0;do{for(var t=0;t<z.length;t+=1){var n=z[t];M(n),at(n.$$)}for(z.length=0;Q.length;)Q.pop()();for(var e=0;e<W.length;e+=1){var r=W[e];rt.has(r)||(rt.add(r),r())}W.length=0}while(z.length);for(;X.length;)X.pop()();tt=!1,et=!1,rt.clear()}}function at(t){if(null!==t.fragment){t.update(),w(t.before_update);var n=t.dirty;t.dirty=[-1],t.fragment&&t.fragment.p(t.ctx,n),t.after_update.forEach(nt)}}var it,ct=new Set;function ut(){it={r:0,c:[],p:it}}function st(){it.r||w(it.c),it=it.p}function ft(t,n){t&&t.i&&(ct.delete(t),t.i(n))}function lt(t,n,e,r){if(t&&t.o){if(ct.has(t))return;ct.add(t),it.c.push((function(){ct.delete(t),r&&(e&&t.d(1),r())})),t.o(n)}}function pt(t,n){for(var e={},r={},o={$$scope:1},a=t.length;a--;){var i=t[a],c=n[a];if(c){for(var u in i)u in c||(r[u]=1);for(var s in c)o[s]||(e[s]=c[s],o[s]=1);t[a]=c}else for(var f in i)o[f]=1}for(var l in r)l in e||(e[l]=void 0);return e}function ht(n){return"object"===t(n)&&null!==n?n:{}}function dt(t){t&&t.c()}function vt(t,n){t&&t.l(n)}function mt(t,n,e){var r=t.$$,o=r.fragment,a=r.on_mount,i=r.on_destroy,c=r.after_update;o&&o.m(n,e),nt((function(){var n=a.map(g).filter(E);i?i.push.apply(i,h(n)):w(n),t.$$.on_mount=[]})),c.forEach(nt)}function yt(t,n){var e=t.$$;null!==e.fragment&&(w(e.on_destroy),e.fragment&&e.fragment.d(n),e.on_destroy=e.fragment=null,e.ctx=[])}function gt(t,n){-1===t.$$.dirty[0]&&(z.push(t),tt||(tt=!0,Z.then(ot)),t.$$.dirty.fill(0)),t.$$.dirty[n/31|0]|=1<<n%31}function bt(t,n,e,r,a,i){var c=arguments.length>6&&void 0!==arguments[6]?arguments[6]:[-1],u=o;M(t);var s=n.props||{},f=t.$$={fragment:null,ctx:null,props:i,update:m,not_equal:a,bound:b(),on_mount:[],on_destroy:[],before_update:[],after_update:[],context:new Map(u?u.$$.context:[]),callbacks:b(),dirty:c},l=!1;if(f.ctx=e?e(t,s,(function(n,e){var r=!(arguments.length<=2)&&arguments.length-2?arguments.length<=2?void 0:arguments[2]:e;return f.ctx&&a(f.ctx[n],f.ctx[n]=r)&&(f.bound[n]&&f.bound[n](r),l&&gt(t,n)),e})):[],f.update(),l=!0,w(f.before_update),f.fragment=!!r&&r(f.ctx),n.target){if(n.hydrate){var p=q(n.target);f.fragment&&f.fragment.l(p),p.forEach(k)}else f.fragment&&f.fragment.c();n.intro&&ft(t.$$.fragment),mt(t,n.target,n.anchor),ot()}M(u)}var wt=function(){function t(){d(this,t)}var n,e,r;return n=t,(e=[{key:"$destroy",value:function(){yt(this,1),this.$destroy=m}},{key:"$on",value:function(t,n){var e=this.$$.callbacks[t]||(this.$$.callbacks[t]=[]);return e.push(n),function(){var t=e.indexOf(n);-1!==t&&e.splice(t,1)}}},{key:"$set",value:function(){}}])&&v(n.prototype,e),r&&v(n,r),t}(),Et=[];function xt(t){var n,e=arguments.length>1&&void 0!==arguments[1]?arguments[1]:m,r=[];function o(e){if(x(t,e)&&(t=e,n)){for(var o=!Et.length,a=0;a<r.length;a+=1){var i=r[a];i[1](),Et.push(i,t)}if(o){for(var c=0;c<Et.length;c+=2)Et[c][0](Et[c+1]);Et.length=0}}}function a(n){o(n(t))}function i(a){var i=arguments.length>1&&void 0!==arguments[1]?arguments[1]:m,c=[a,i];return r.push(c),1===r.length&&(n=e(o)||m),a(t),function(){var t=r.indexOf(c);-1!==t&&r.splice(t,1),0===r.length&&(n(),n=null)}}return{set:o,update:a,subscribe:i}}var $t={},_t=function(){return{}};var St=function(){var t=xt(null),n=t.subscribe,e=t.update;return{subscribe:n,set:function(t){return e((function(n){return t}))}}}(),Lt=function(){var t=xt(null),n=t.subscribe,e=t.set,r=t.update;return{subscribe:n,init:function(t){return function(t){e(t)}(t)},set:function(t){return r((function(n){return t}))}}}(),At=xt(!1),Rt={email:xt(null),password:xt(null),logued:xt(!1)},Pt=function(){var t=c(a.mark((function t(n){return a.wrap((function(t){for(;;)switch(t.prev=t.next){case 0:Rt.email.set(n.email),Rt.password.set(n.password),At.set(!1);case 3:case"end":return t.stop()}}),t)})));return function(n){return t.apply(this,arguments)}}(),kt=function(){var t=c(a.mark((function t(){return a.wrap((function(t){for(;;)switch(t.prev=t.next){case 0:Rt.logued.set(!0);case 1:case"end":return t.stop()}}),t)})));return function(){return t.apply(this,arguments)}}();function jt(t){var n=function(){if("undefined"==typeof Reflect||!Reflect.construct)return!1;if(Reflect.construct.sham)return!1;if("function"==typeof Proxy)return!0;try{return Date.prototype.toString.call(Reflect.construct(Date,[],(function(){}))),!0}catch(t){return!1}}();return function(){var e,r=u(t);if(n){var o=u(this).constructor;e=Reflect.construct(r,arguments,o)}else e=r.apply(this,arguments);return p(this,e)}}function Ot(t){var n,e,o,a,i,c,u,s,f,l,p,h,d,v,y,g,b,w,E,x,$,_,S,L,A,I,D,H,V,B=t[1].alias+"";return{c:function(){n=j("div"),e=j("ul"),o=j("li"),a=j("a"),i=O(B),c=C(),u=j("li"),s=j("a"),f=O("Identidad"),l=C(),p=j("li"),h=j("a"),d=O("Actividades"),v=C(),y=j("li"),g=j("a"),b=O("Usuario"),w=C(),E=j("li"),x=j("a"),$=O("Login"),_=C(),S=j("li"),L=j("a"),A=O("Salas"),I=C(),D=j("a"),H=j("input"),this.h()},l:function(t){var r=q(n=U(t,"DIV",{id:!0,class:!0})),m=q(e=U(r,"UL",{class:!0})),R=q(o=U(m,"LI",{class:!0})),P=q(a=U(R,"A",{href:!0}));i=T(P,B),P.forEach(k),R.forEach(k),c=G(m);var j=q(u=U(m,"LI",{class:!0})),O=q(s=U(j,"A",{href:!0}));f=T(O,"Identidad"),O.forEach(k),j.forEach(k),l=G(m);var C=q(p=U(m,"LI",{class:!0})),N=q(h=U(C,"A",{href:!0}));d=T(N,"Actividades"),N.forEach(k),C.forEach(k),v=G(m);var F=q(y=U(m,"LI",{class:!0})),V=q(g=U(F,"A",{href:!0}));b=T(V,"Usuario"),V.forEach(k),F.forEach(k),w=G(m);var J=q(E=U(m,"LI",{class:!0})),M=q(x=U(J,"A",{href:!0}));$=T(M,"Login"),M.forEach(k),J.forEach(k),_=G(m);var K=q(S=U(m,"LI",{class:!0})),Y=q(L=U(K,"A",{href:!0}));A=T(Y,"Salas"),Y.forEach(k),K.forEach(k),m.forEach(k),r.forEach(k),I=G(t);var z=q(D=U(t,"A",{class:!0,href:!0}));H=U(z,"INPUT",{type:!0,name:!0}),z.forEach(k),this.h()},h:function(){N(a,"href","."),N(o,"class","menu-item"),N(s,"href","club"),N(u,"class","menu-item"),N(h,"href","activity"),N(p,"class","menu-item"),N(g,"href","user"),N(y,"class","menu-item"),N(x,"href","login"),N(E,"class","menu-item"),N(L,"href","room"),N(S,"class","menu-item"),N(e,"class","menu svelte-1sg4mug"),N(n,"id","sidebar-id"),N(n,"class","off-canvas-sidebar"),N(H,"type","hidden"),N(H,"name","hidden"),N(D,"class","off-canvas-overlay"),N(D,"href",V=(null!=t[0]?t[0]:"")+"#close")},m:function(t,r){P(t,n,r),R(n,e),R(e,o),R(o,a),R(a,i),R(e,c),R(e,u),R(u,s),R(s,f),R(e,l),R(e,p),R(p,h),R(h,d),R(e,v),R(e,y),R(y,g),R(g,b),R(e,w),R(e,E),R(E,x),R(x,$),R(e,_),R(e,S),R(S,L),R(L,A),P(t,I,r),P(t,D,r),R(D,H)},p:function(t,n){var e=r(n,1)[0];2&e&&B!==(B=t[1].alias+"")&&F(i,B),1&e&&V!==(V=(null!=t[0]?t[0]:"")+"#close")&&N(D,"href",V)},i:m,o:m,d:function(t){t&&k(n),t&&k(I),t&&k(D)}}}function Ct(t,n,e){var r;$(t,St,(function(t){return e(1,r=t)}));var o=n.segment;return t.$set=function(t){"segment"in t&&e(0,o=t.segment)},[o,r]}var It=function(t){f(e,wt);var n=jt(e);function e(t){var r;return d(this,e),bt(l(r=n.call(this)),t,Ct,Ot,x,{segment:0}),r}return e}();function Dt(t){var n=function(){if("undefined"==typeof Reflect||!Reflect.construct)return!1;if(Reflect.construct.sham)return!1;if("function"==typeof Proxy)return!0;try{return Date.prototype.toString.call(Reflect.construct(Date,[],(function(){}))),!0}catch(t){return!1}}();return function(){var e,r=u(t);if(n){var o=u(this).constructor;e=Reflect.construct(r,arguments,o)}else e=r.apply(this,arguments);return p(this,e)}}function Nt(t){var n,e;return n=new It({props:{segment:t[0]}}),{c:function(){dt(n.$$.fragment)},l:function(t){vt(n.$$.fragment,t)},m:function(t,r){mt(n,t,r),e=!0},p:function(t,e){var r={};1&e&&(r.segment=t[0]),n.$set(r)},i:function(t){e||(ft(n.$$.fragment,t),e=!0)},o:function(t){lt(n.$$.fragment,t),e=!1},d:function(t){yt(n,t)}}}function qt(t){var n,e,r,o,a,i,c,u,s=t[2].alias+"";return{c:function(){n=j("header"),e=j("section"),r=j("a"),o=j("i"),i=C(),c=j("a"),u=O(s),this.h()},l:function(t){var a=q(n=U(t,"HEADER",{class:!0})),f=q(e=U(a,"SECTION",{class:!0})),l=q(r=U(f,"A",{class:!0,href:!0}));q(o=U(l,"I",{class:!0})).forEach(k),l.forEach(k),i=G(f);var p=q(c=U(f,"A",{href:!0,class:!0}));u=T(p,s),p.forEach(k),f.forEach(k),a.forEach(k),this.h()},h:function(){N(o,"class","icon icon-menu"),N(r,"class","off-canvas-toggle btn btn-primary btn-action"),N(r,"href",a=(null!=t[0]?t[0]:"")+"#sidebar-id"),N(c,"href","."),N(c,"class","btn btn-link"),N(e,"class","navbar-section"),N(n,"class","navbar show-xs")},m:function(t,a){P(t,n,a),R(n,e),R(e,r),R(r,o),R(e,i),R(e,c),R(c,u)},p:function(t,n){1&n&&a!==(a=(null!=t[0]?t[0]:"")+"#sidebar-id")&&N(r,"href",a),4&n&&s!==(s=t[2].alias+"")&&F(u,s)},d:function(t){t&&k(n)}}}function Ut(t){var n,e,o,a,i,c,u,s,f=t[1]&&Nt(t),l=t[1]&&qt(t),p=t[4].default,h=_(p,t,t[3],null);return{c:function(){n=j("div"),f&&f.c(),e=C(),o=j("div"),a=j("div"),i=j("div"),c=j("div"),l&&l.c(),u=C(),h&&h.c(),this.h()},l:function(t){var r=q(n=U(t,"DIV",{class:!0}));f&&f.l(r),e=G(r);var s=q(o=U(r,"DIV",{class:!0})),p=q(a=U(s,"DIV",{class:!0})),d=q(i=U(p,"DIV",{class:!0})),v=q(c=U(d,"DIV",{class:!0}));l&&l.l(v),u=G(v),h&&h.l(v),v.forEach(k),d.forEach(k),p.forEach(k),s.forEach(k),r.forEach(k),this.h()},h:function(){N(c,"class","main column col-12 col-mx-auto"),N(i,"class","columns"),N(a,"class","container grid-lg"),N(o,"class","off-canvas-content"),N(n,"class","off-canvas off-canvas-sidebar-show")},m:function(t,r){P(t,n,r),f&&f.m(n,null),R(n,e),R(n,o),R(o,a),R(a,i),R(i,c),l&&l.m(c,null),R(c,u),h&&h.m(c,null),s=!0},p:function(t,o){var a=r(o,1)[0];t[1]?f?(f.p(t,a),2&a&&ft(f,1)):((f=Nt(t)).c(),ft(f,1),f.m(n,e)):f&&(ut(),lt(f,1,1,(function(){f=null})),st()),t[1]?l?l.p(t,a):((l=qt(t)).c(),l.m(c,u)):l&&(l.d(1),l=null),h&&h.p&&8&a&&L(h,p,t,t[3],a,null,null)},i:function(t){s||(ft(f),ft(h,t),s=!0)},o:function(t){lt(f),lt(h,t),s=!1},d:function(t){t&&k(n),f&&f.d(),l&&l.d(),h&&h.d(t)}}}function Tt(t,n,e){var r;$(t,St,(function(t){return e(2,r=t)}));var o,i=!1;o=function(){Rt.logued.subscribe(function(){var t=c(a.mark((function t(n){return a.wrap((function(t){for(;;)switch(t.prev=t.next){case 0:if(e(1,i=n),!i){t.next=6;break}return t.next=4,en("/user");case 4:t.next=8;break;case 6:return t.next=8,en("/login");case 8:case"end":return t.stop()}}),t)})));return function(n){return t.apply(this,arguments)}}())},K().$$.on_mount.push(o);var u=n.segment,s=n.$$slots,f=void 0===s?{}:s,l=n.$$scope;return t.$set=function(t){"segment"in t&&e(0,u=t.segment),"$$scope"in t&&e(3,l=t.$$scope)},[u,i,r,l,f]}var Gt=function(t){f(e,wt);var n=Dt(e);function e(t){var r;return d(this,e),bt(l(r=n.call(this)),t,Tt,Ut,x,{segment:0}),r}return e}();function Ft(t){var n=function(){if("undefined"==typeof Reflect||!Reflect.construct)return!1;if(Reflect.construct.sham)return!1;if("function"==typeof Proxy)return!0;try{return Date.prototype.toString.call(Reflect.construct(Date,[],(function(){}))),!0}catch(t){return!1}}();return function(){var e,r=u(t);if(n){var o=u(this).constructor;e=Reflect.construct(r,arguments,o)}else e=r.apply(this,arguments);return p(this,e)}}function Ht(t){var n,e,r=t[1].stack+"";return{c:function(){n=j("pre"),e=O(r)},l:function(t){var o=q(n=U(t,"PRE",{}));e=T(o,r),o.forEach(k)},m:function(t,r){P(t,n,r),R(n,e)},p:function(t,n){2&n&&r!==(r=t[1].stack+"")&&F(e,r)},d:function(t){t&&k(n)}}}function Vt(t){var n,e,o,a,i,c,u,s,f,l=t[1].message+"";document.title=n=t[0];var p=t[2]&&t[1].stack&&Ht(t);return{c:function(){e=C(),o=j("h1"),a=O(t[0]),i=C(),c=j("p"),u=O(l),s=C(),p&&p.c(),f=I(),this.h()},l:function(n){J('[data-svelte="svelte-1o9r2ue"]',document.head).forEach(k),e=G(n);var r=q(o=U(n,"H1",{class:!0}));a=T(r,t[0]),r.forEach(k),i=G(n);var h=q(c=U(n,"P",{class:!0}));u=T(h,l),h.forEach(k),s=G(n),p&&p.l(n),f=I(),this.h()},h:function(){N(o,"class","svelte-8od9u6"),N(c,"class","svelte-8od9u6")},m:function(t,n){P(t,e,n),P(t,o,n),R(o,a),P(t,i,n),P(t,c,n),R(c,u),P(t,s,n),p&&p.m(t,n),P(t,f,n)},p:function(t,e){var o=r(e,1)[0];1&o&&n!==(n=t[0])&&(document.title=n),1&o&&F(a,t[0]),2&o&&l!==(l=t[1].message+"")&&F(u,l),t[2]&&t[1].stack?p?p.p(t,o):((p=Ht(t)).c(),p.m(f.parentNode,f)):p&&(p.d(1),p=null)},i:m,o:m,d:function(t){t&&k(e),t&&k(o),t&&k(i),t&&k(c),t&&k(s),p&&p.d(t),t&&k(f)}}}function Bt(t,n,e){var r=n.status,o=n.error;return t.$set=function(t){"status"in t&&e(0,r=t.status),"error"in t&&e(1,o=t.error)},[r,o,!1]}var Jt=function(t){f(e,wt);var n=Ft(e);function e(t){var r;return d(this,e),bt(l(r=n.call(this)),t,Bt,Vt,x,{status:0,error:1}),r}return e}();function Mt(t){var n=function(){if("undefined"==typeof Reflect||!Reflect.construct)return!1;if(Reflect.construct.sham)return!1;if("function"==typeof Proxy)return!0;try{return Date.prototype.toString.call(Reflect.construct(Date,[],(function(){}))),!0}catch(t){return!1}}();return function(){var e,r=u(t);if(n){var o=u(this).constructor;e=Reflect.construct(r,arguments,o)}else e=r.apply(this,arguments);return p(this,e)}}function Kt(t){var n,e,r,o=[t[4].props],a=t[4].component;function i(t){for(var n={},e=0;e<o.length;e+=1)n=y(n,o[e]);return{props:n}}return a&&(n=new a(i())),{c:function(){n&&dt(n.$$.fragment),e=I()},l:function(t){n&&vt(n.$$.fragment,t),e=I()},m:function(t,o){n&&mt(n,t,o),P(t,e,o),r=!0},p:function(t,r){var c=16&r?pt(o,[ht(t[4].props)]):{};if(a!==(a=t[4].component)){if(n){ut();var u=n;lt(u.$$.fragment,1,0,(function(){yt(u,1)})),st()}a?(dt((n=new a(i())).$$.fragment),ft(n.$$.fragment,1),mt(n,e.parentNode,e)):n=null}else a&&n.$set(c)},i:function(t){r||(n&&ft(n.$$.fragment,t),r=!0)},o:function(t){n&&lt(n.$$.fragment,t),r=!1},d:function(t){t&&k(e),n&&yt(n,t)}}}function Yt(t){var n,e;return n=new Jt({props:{error:t[0],status:t[1]}}),{c:function(){dt(n.$$.fragment)},l:function(t){vt(n.$$.fragment,t)},m:function(t,r){mt(n,t,r),e=!0},p:function(t,e){var r={};1&e&&(r.error=t[0]),2&e&&(r.status=t[1]),n.$set(r)},i:function(t){e||(ft(n.$$.fragment,t),e=!0)},o:function(t){lt(n.$$.fragment,t),e=!1},d:function(t){yt(n,t)}}}function zt(t){var n,e,r,o,a=[Yt,Kt],i=[];function c(t,n){return t[0]?0:1}return n=c(t),e=i[n]=a[n](t),{c:function(){e.c(),r=I()},l:function(t){e.l(t),r=I()},m:function(t,e){i[n].m(t,e),P(t,r,e),o=!0},p:function(t,o){var u=n;(n=c(t))===u?i[n].p(t,o):(ut(),lt(i[u],1,1,(function(){i[u]=null})),st(),(e=i[n])||(e=i[n]=a[n](t)).c(),ft(e,1),e.m(r.parentNode,r))},i:function(t){o||(ft(e),o=!0)},o:function(t){lt(e),o=!1},d:function(t){i[n].d(t),t&&k(r)}}}function Qt(t){for(var n,e,o=[{segment:t[2][0]},t[3].props],a={$$slots:{default:[zt]},$$scope:{ctx:t}},i=0;i<o.length;i+=1)a=y(a,o[i]);return n=new Gt({props:a}),{c:function(){dt(n.$$.fragment)},l:function(t){vt(n.$$.fragment,t)},m:function(t,r){mt(n,t,r),e=!0},p:function(t,e){var a=r(e,1)[0],i=12&a?pt(o,[4&a&&{segment:t[2][0]},8&a&&ht(t[3].props)]):{};147&a&&(i.$$scope={dirty:a,ctx:t}),n.$set(i)},i:function(t){e||(ft(n.$$.fragment,t),e=!0)},o:function(t){lt(n.$$.fragment,t),e=!1},d:function(t){yt(n,t)}}}function Wt(t,n,e){var r,o,a,i=n.stores,c=n.error,u=n.status,s=n.segments,f=n.level0,l=n.level1,p=void 0===l?null:l,h=n.notify;return r=h,K().$$.after_update.push(r),o=$t,a=i,K().$$.context.set(o,a),t.$set=function(t){"stores"in t&&e(5,i=t.stores),"error"in t&&e(0,c=t.error),"status"in t&&e(1,u=t.status),"segments"in t&&e(2,s=t.segments),"level0"in t&&e(3,f=t.level0),"level1"in t&&e(4,p=t.level1),"notify"in t&&e(6,h=t.notify)},[c,u,s,f,p,i,h]}var Xt=function(t){f(e,wt);var n=Mt(e);function e(t){var r;return d(this,e),bt(l(r=n.call(this)),t,Wt,Qt,x,{stores:5,error:0,status:1,segments:2,level0:3,level1:4,notify:6}),r}return e}(),Zt=[],tn=[{js:function(){return import("./index.0c7b6f29.js")},css:["legacy/client.3e3efd06.css"]},{js:function(){return import("./activity.6b7f88a8.js")},css:["legacy/activity.6b7f88a8.css","legacy/client.3e3efd06.css"]},{js:function(){return import("./about.9434c6dd.js")},css:["legacy/client.3e3efd06.css"]},{js:function(){return import("./login.36764b80.js")},css:["legacy/client.3e3efd06.css"]},{js:function(){return import("./club.7973ce9e.js")},css:["legacy/club.7973ce9e.css","legacy/client.3e3efd06.css"]},{js:function(){return import("./room.23425de8.js")},css:["legacy/client.3e3efd06.css"]},{js:function(){return import("./user.aa31d7cb.js")},css:["legacy/client.3e3efd06.css"]}],nn=[{pattern:/^\/$/,parts:[{i:0}]},{pattern:/^\/activity\/?$/,parts:[{i:1}]},{pattern:/^\/about\/?$/,parts:[{i:2}]},{pattern:/^\/login\/?$/,parts:[{i:3}]},{pattern:/^\/club\/?$/,parts:[{i:4}]},{pattern:/^\/room\/?$/,parts:[{i:5}]},{pattern:/^\/user\/?$/,parts:[{i:6}]}];function en(t){var n=arguments.length>1&&void 0!==arguments[1]?arguments[1]:{replaceState:!1},e=xn(new URL(t,document.baseURI));return e?(bn[n.replaceState?"replaceState":"pushState"]({id:mn},"",t),_n(e,null).then((function(){}))):(location.href=t,new Promise((function(t){})))}var rn,on,an,cn,un,sn="undefined"!=typeof __SAPPER__&&__SAPPER__,fn=!1,ln=[],pn="{}",hn={page:function(t){var n=xt(t),e=!0;return{notify:function(){e=!0,n.update((function(t){return t}))},set:function(t){e=!1,n.set(t)},subscribe:function(t){var r;return n.subscribe((function(n){(void 0===r||e&&n!==r)&&t(r=n)}))}}}({}),preloading:xt(null),session:xt(sn&&sn.session)};hn.session.subscribe(function(){var t=c(a.mark((function t(n){var e,r,o,i,c,u;return a.wrap((function(t){for(;;)switch(t.prev=t.next){case 0:if(cn=n,fn){t.next=3;break}return t.abrupt("return");case 3:return un=!0,e=xn(new URL(location.href)),r=on={},t.next=8,Pn(e);case 8:if(o=t.sent,i=o.redirect,c=o.props,u=o.branch,r===on){t.next=14;break}return t.abrupt("return");case 14:return t.next=16,Ln(i,u,c,e.page);case 16:case"end":return t.stop()}}),t)})));return function(n){return t.apply(this,arguments)}}());var dn,vn=null;var mn,yn=1;var gn,bn="undefined"!=typeof history?history:{pushState:function(t,n,e){},replaceState:function(t,n,e){},scrollRestoration:""},wn={};function En(n){var e=Object.create(null);return n.length>0&&n.slice(1).split("&").forEach((function(n){var o=r(/([^=]*)(?:=(.*))?/.exec(decodeURIComponent(n.replace(/\+/g," "))),3),a=o[1],i=o[2],c=void 0===i?"":i;"string"==typeof e[a]&&(e[a]=[e[a]]),"object"===t(e[a])?e[a].push(c):e[a]=c})),e}function xn(t){if(t.origin!==location.origin)return null;if(!t.pathname.startsWith(sn.baseUrl))return null;var n=t.pathname.slice(sn.baseUrl.length);if(""===n&&(n="/"),!Zt.some((function(t){return t.test(n)})))for(var e=0;e<nn.length;e+=1){var r=nn[e],o=r.pattern.exec(n);if(o){var a=En(t.search),i=r.parts[r.parts.length-1],c=i.params?i.params(o):{},u={host:location.host,path:n,query:a,params:c};return{href:t.href,route:r,match:o,page:u}}}}function $n(){return{x:pageXOffset,y:pageYOffset}}function _n(t,n,e,r){return Sn.apply(this,arguments)}function Sn(){return(Sn=c(a.mark((function t(n,e,r,o){var i,c,u,s,f,l,p,h,d;return a.wrap((function(t){for(;;)switch(t.prev=t.next){case 0:return e?mn=e:(i=$n(),wn[mn]=i,e=mn=++yn,wn[mn]=r?i:{x:0,y:0}),mn=e,rn&&hn.preloading.set(!0),c=vn&&vn.href===n.href?vn.promise:Pn(n),vn=null,u=on={},t.next=8,c;case 8:if(s=t.sent,f=s.redirect,l=s.props,p=s.branch,u===on){t.next=14;break}return t.abrupt("return");case 14:return t.next=16,Ln(f,p,l,n.page);case 16:document.activeElement&&document.activeElement.blur(),r||(h=wn[e],o&&(d=document.getElementById(o.slice(1)))&&(h={x:0,y:d.getBoundingClientRect().top+scrollY}),wn[mn]=h,h&&scrollTo(h.x,h.y));case 18:case"end":return t.stop()}}),t)})))).apply(this,arguments)}function Ln(t,n,e,r){return An.apply(this,arguments)}function An(){return(An=c(a.mark((function t(n,e,r,o){var i,c;return a.wrap((function(t){for(;;)switch(t.prev=t.next){case 0:if(!n){t.next=2;break}return t.abrupt("return",en(n.location,{replaceState:!0}));case 2:if(hn.page.set(o),hn.preloading.set(!1),!rn){t.next=8;break}rn.$set(r),t.next=18;break;case 8:return r.stores={page:{subscribe:hn.page.subscribe},preloading:{subscribe:hn.preloading.subscribe},session:hn.session},t.next=11,an;case 11:if(t.t0=t.sent,r.level0={props:t.t0},r.notify=hn.page.notify,i=document.querySelector("#sapper-head-start"),c=document.querySelector("#sapper-head-end"),i&&c){for(;i.nextSibling!==c;)Cn(i.nextSibling);Cn(i),Cn(c)}rn=new Xt({target:dn,props:r,hydrate:!0});case 18:ln=e,pn=JSON.stringify(o.query),fn=!0,un=!1;case 22:case"end":return t.stop()}}),t)})))).apply(this,arguments)}function Rn(t,n,e,r){if(r!==pn)return!0;var o=ln[t];return!!o&&(n!==o.segment||(!(!o.match||JSON.stringify(o.match.slice(1,t+2))===JSON.stringify(e.slice(1,t+2)))||void 0))}function Pn(t){return kn.apply(this,arguments)}function kn(){return(kn=c(a.mark((function t(n){var e,r,o,i,u,s,f,l,p,h,d;return a.wrap((function(t){for(;;)switch(t.prev=t.next){case 0:return e=n.route,r=n.page,o=r.path.split("/").filter(Boolean),i=null,u={error:null,status:200,segments:[o[0]]},s={fetch:function(t){function n(n,e){return t.apply(this,arguments)}return n.toString=function(){return t.toString()},n}((function(t,n){return fetch(t,n)})),redirect:function(t,n){if(i&&(i.statusCode!==t||i.location!==n))throw new Error("Conflicting redirects");i={statusCode:t,location:n}},error:function(t,n){u.error="string"==typeof n?new Error(n):n,u.status=t}},an||(an=sn.preloaded[0]||_t.call(s,{host:r.host,path:r.path,query:r.query,params:{}},cn)),l=1,t.prev=7,p=JSON.stringify(r.query),h=e.pattern.exec(r.path),d=!1,t.next=13,Promise.all(e.parts.map(function(){var t=c(a.mark((function t(e,i){var c,f,v,m,y,g;return a.wrap((function(t){for(;;)switch(t.prev=t.next){case 0:if(c=o[i],Rn(i,c,h,p)&&(d=!0),u.segments[l]=o[i+1],e){t.next=5;break}return t.abrupt("return",{segment:c});case 5:if(f=l++,un||d||!ln[i]||ln[i].part!==e.i){t.next=8;break}return t.abrupt("return",ln[i]);case 8:return d=!1,t.next=11,On(tn[e.i]);case 11:if(v=t.sent,m=v.default,y=v.preload,!fn&&sn.preloaded[i+1]){t.next=25;break}if(!y){t.next=21;break}return t.next=18,y.call(s,{host:r.host,path:r.path,query:r.query,params:e.params?e.params(n.match):{}},cn);case 18:t.t0=t.sent,t.next=22;break;case 21:t.t0={};case 22:g=t.t0,t.next=26;break;case 25:g=sn.preloaded[i+1];case 26:return t.abrupt("return",u["level".concat(f)]={component:m,props:g,segment:c,match:h,part:e.i});case 27:case"end":return t.stop()}}),t)})));return function(n,e){return t.apply(this,arguments)}}()));case 13:f=t.sent,t.next=21;break;case 16:t.prev=16,t.t0=t.catch(7),u.error=t.t0,u.status=500,f=[];case 21:return t.abrupt("return",{redirect:i,props:u,branch:f});case 22:case"end":return t.stop()}}),t,null,[[7,16]])})))).apply(this,arguments)}function jn(t){var n="client/".concat(t);if(!document.querySelector('link[href="'.concat(n,'"]')))return new Promise((function(t,e){var r=document.createElement("link");r.rel="stylesheet",r.href=n,r.onload=function(){return t()},r.onerror=e,document.head.appendChild(r)}))}function On(t){var n="string"==typeof t.css?[]:t.css.map(jn);return n.unshift(t.js()),Promise.all(n).then((function(t){return t[0]}))}function Cn(t){t.parentNode.removeChild(t)}function In(t){var n=xn(new URL(t,document.baseURI));if(n)return vn&&t===vn.href||function(t,n){vn={href:t,promise:n}}(t,Pn(n)),vn.promise}function Dn(t){clearTimeout(gn),gn=setTimeout((function(){Nn(t)}),20)}function Nn(t){var n=Un(t.target);n&&"prefetch"===n.rel&&In(n.href)}function qn(n){if(1===function(t){return null===t.which?t.button:t.which}(n)&&!(n.metaKey||n.ctrlKey||n.shiftKey||n.defaultPrevented)){var e=Un(n.target);if(e&&e.href){var r="object"===t(e.href)&&"SVGAnimatedString"===e.href.constructor.name,o=String(r?e.href.baseVal:e.href);if(o!==location.href){if(!e.hasAttribute("download")&&"external"!==e.getAttribute("rel")&&!(r?e.target.baseVal:e.target)){var a=new URL(o);if(a.pathname!==location.pathname||a.search!==location.search){var i=xn(a);if(i)_n(i,null,e.hasAttribute("sapper-noscroll"),a.hash),n.preventDefault(),bn.pushState({id:mn},"",a.href)}}}else location.hash||n.preventDefault()}}}function Un(t){for(;t&&"A"!==t.nodeName.toUpperCase();)t=t.parentNode;return t}function Tn(t){if(wn[mn]=$n(),t.state){var n=xn(new URL(location.href));n?_n(n,t.state.id):location.href=location.href}else(function(t){mn=t})(yn=yn+1),bn.replaceState({id:mn},"",location.href)}var Gn;St.set({name:"Club Altetico Stentor",short:"CAS",alias:"CAStentor",direction:"Gascon 1111",phone:"4444-4444",image:"https://via.placeholder.com/976x250",bio:"El club del Barrio ❤️ #VillaLuro\nBaby fútbol #Futsal Femenino y Masculino, Gimnasio y más!",history:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."}),Lt.init({name:"Futbol infantil",type:"sport_futbol",Bio:"Profesora: Giselle Lorena Alarcon\nPrincipiantes, intermedias y avanzadas"}),Gn={target:document.querySelector("#sapper")},"scrollRestoration"in bn&&(bn.scrollRestoration="manual"),addEventListener("beforeunload",(function(){bn.scrollRestoration="auto"})),addEventListener("load",(function(){bn.scrollRestoration="manual"})),function(t){dn=t}(Gn.target),addEventListener("click",qn),addEventListener("popstate",Tn),addEventListener("touchstart",Nn),addEventListener("mousemove",Dn),Promise.resolve().then((function(){var t=location,n=t.hash,e=t.href;bn.replaceState({id:yn},"",e);var r,o,a,i,c,u,s,f,l=new URL(location.href);if(sn.error)return r=location,o=r.host,a=r.pathname,i=r.search,c=sn.session,u=sn.preloaded,s=sn.status,f=sn.error,an||(an=u&&u[0]),void Ln(null,[],{error:f,status:s,session:c,level0:{props:an},level1:{props:{status:s,error:f},component:Jt},segments:u},{host:o,path:a,query:En(i),params:{}});var p=xn(l);return p?_n(p,yn,!0,n):void 0}));export{lt as A,A as B,ut as C,st as D,Y as E,_ as F,dt as G,vt as H,mt as I,yt as J,B as K,Lt as L,H as M,D as N,w as O,Rt as P,Pt as Q,kt as R,wt as S,f as _,u as a,p as b,d as c,l as d,C as e,j as f,k as g,G as h,bt as i,U as j,q as k,T as l,N as m,V as n,P as o,R as p,J as q,r,x as s,O as t,F as u,m as v,$ as w,St as x,L as y,ft as z};
