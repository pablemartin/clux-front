import { writable, get } from 'svelte/store';

export const api = writable({
  trans: 'http://',
  host: `localhost:8000`,
  port: '8000',
  access_token: null,
  token_type: null,
  paths: {
    token: '/token',
    club: '/',
    user: '/users/me/'
  }
})

const User = {
  username: null,
  password: null,
  logued: false,
  name: null,
  email: null,
  bio: null,
  avatar: null,
  access_token: null,
  token_type: null
}

const Club = {
  name: null,
  short: null,
  alias: null,
  direction: null,
  phone: null,
  image: null,
  bio: null,
  history: null
}

const Activity = {
  name: null,
  type: null,
  Bio: null
}

function user_init(user) {
  const { subscribe, set, update } = writable(null);

  update(data => user)

  const login = async () => {
    user.logued = true
  };

  return {
    subscribe,
    login: () => login(),
    init: (user) => set(user),
    set: (user) => update(data => { return {...data,...user} })
  }
}

function activity_init(activity) {
  const { subscribe, set, update } = writable(null);

  return {
    subscribe,
    init: (activity) => set(activity),
    set: (activity) => update(data => activity)
  };
}

function club_init(club) {
  const { subscribe, set, update } = writable(null);

  update(data => club)

  return {
    subscribe,
    set: (club) => update(data => club)
  };
}

export const user = user_init(User);
export const club = club_init(Club);
export const activity = activity_init(Activity);

export const tokenize = async () => {
  
  let suser = get(user)
  let sapi = get(api)
  
  const res = await fetch(sapi.trans + sapi.host + sapi.paths.token,{
      method: 'POST',
      mode: 'cors',
      credentials: 'same-origin',
      headers: {
        "accept": "application/json",
            'Content-Type': 'application/x-www-form-urlencoded'
        },
      body: "username="+suser.username+"&password="+suser.password
    })
  const data = await res.json()
  
  user.set(data)
  localStorage.setItem('system_user', get(user).username)
  api.set({...get(api), ...data})

  console.log("Tokenize...ok!")
}

export const get_user_profile = async () => {
  
  let suser = get(user)
  let sapi = get(api)
  
  let res = await fetch(sapi.trans + sapi.host + sapi.paths.user,{
    headers: {
      "accept": "application/json",
      'Authorization': 'Bearer ' + sapi.access_token
    }
  });
  
  let user_data = await res.json()
  
  user.set({
    'email': user_data.email, 
    'name': user_data.email,
    'username': user_data.username,
  })
}

export const get_club = async () => {
  
  let tmp_club = get(club)
  let suser = get(user)
  let sapi = get(api)
  let res = null
  let data = null

  // if (tmp_club.name == null) {
  //   res = await fetch(sapi.trans + sapi.host + sapi.paths.club);
  //   data = await res.json() 
  // }

  // if (data){
  //   club.set(data.club)
  //   activity.set(data.club.activities[0])   
  // }

  tmp_club = {
        "name": 'Club Altetico Stentor',
        "short": 'CAS',
        "alias": "cas",
        "direction": "Gascon 1111",
        "phone": "4444-4444",
        "image": 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fi.ytimg.com%2Fvi%2FCyp5sVTJa6c%2Fmaxresdefault.jpg&f=1&nofb=1',
        "logo": 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.7V73aT-OcdNxVpRqSq-RDAHaHa%26pid%3DApi&f=1',
        "bio": 'El club del Barrio ❤️ #VillaLuro\nBaby fútbol #Futsal Femenino y Masculino, Gimnasio y más!',
        "history": 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        "activities": [{
            "name": "Futbol infantil",
            "type": "sport_futbol",
            "Bio": "Profesora: Giselle Lorena Alarcon\nPrincipiantes, intermedias y avanzadas"
        }]
  }

  club.set(tmp_club)
  activity.set(tmp_club.activities[0])
}

export const set_host = (host) => {
  api.set({...get(api), ...{ host: host.replace('3000', get(api).port )}})
}


async function refresh () {
  await tokenize()
}

// setInterval(refresh, 1000*60)